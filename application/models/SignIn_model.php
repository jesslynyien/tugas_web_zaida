<?php defined('BASEPATH') OR exit('No direct script access allowed');

class SignIn_model extends CI_Model
{
    private $_table = "pengguna";

    private $ID;
    private $Nama;
    private $Email;
    private $Pass;

    public function rules()
    {
        return [
            ['field' => 'nama',
            'label' => 'Nama',
            'rules' => 'required'],

            ['field' => 'email',
            'label' => 'Email',
            'rules' => 'email'],
            
            ['field' => 'pass',
            'label' => 'Pass',
            'rules' => 'required']
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["ID" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->ID = uniqid();
        $this->Nama = $post["Nama"];
        $this->Email = $post["Email"];
        $this->Pass = $post["Pass"];
        $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->ID = $post["ID"];
        $this->Nama = $post["Nama"];
        $this->Email = $post["Email"];
        $this->Pass = $post["Pass"];
        $this->db->update($this->_table, $this, array('ID' => $post['ID']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("ID" => $id));
    }
}

