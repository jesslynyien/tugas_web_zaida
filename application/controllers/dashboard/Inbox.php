<?php 



defined('BASEPATH') OR exit('No direct script access allowed');

class Inbox extends CI_Controller {

    private $data = [];


    // MASUKIN NAMA TABLE YANG DI DATABASE
    private $table = 'inbox';

    // MASUKIN NAMA PRIMARY KEY YANG DARI DATABASE
    private $pk = 'id_inbox';

    public function __construct() {
        parent::__construct();
    }
    
    public function index()
    {

        $this->data['id'] = $this->input->get('id');
        $this->data['submit_url']  = 'dashboard/inbox/add';
        $this->data['data'] = $this->db->select()->from($this->table)->get();
        if($this->data['id']) 
        {
            $this->data['row'] = $this->find($this->data['id']);
            $this->data['submit_url'] = 'dashboard/inbox/update';
        }

        $this->load->view('admin/header');

        // HEADER SAMA FOOTER JANGAN DI GANTI 
        // YANG INI AJA, SESUAI KAN SAMA VIEW YANG DIBUAT DI FOLDER "APPLICAITON/VIEWS/ADMIN/{NAMA CIBTRIKKER}/NAMA FIILE"
        $this->load->View('admin/inbox', $this->data);
        $this->load->view('admin/footer');
    }

    public function find( $value ) {
        $query = $this->db->select()
            ->from($this->table)
            ->where($this->pk, $value)
            ->get();
        
        if($query->num_rows() > 0 ) {
            return $query->row();
        }

        return false;
    }

    public function delete()
    {

        $response = [];

        $id = $this->input->get('id');
        $find = $this->find($id);

        

        if($find) {
            
            $this->db->where($this->pk, $id);
            $this->db->delete($this->table);
            //die();


            
            $response = [
                'code' => 200,
                'status' => 'success',
                'message' => 'Berhasil menghapus'
            ];

        }else {
            $response = [
                'code' => 400,
                'status' => 'warning',
                'message' => 'Tidak dapat menghapus'
            ];
        }

        $this->session->set_flashdata($response);
        
        
        //GANTI REDIRECT SESUAI CONTROLLERNYA
        redirect(base_url('/dashboard/inbox'));

    }

    public function add()
    {

        // GANTI INPUT SESUAI FORM 
        $data = [
            'fullname' => $this->input->post('fullname'),            
            'email' => $this->input->post('email'),
            'subject'=> $this->input->post('subject'),
            'message'=> $this->input->post('message'),
        ];

        $this->db->insert($this->table, $data);

        $response = [
            'code' => 200,
            'status' => 'success',
            'message' => 'Berhasil menambahkan'
        ];

        $this->session->set_flashdata( $response );
        
        
        //GANTI REDIRECT SESUAI CONTROLLERNYA
        redirect(base_url('dashboard/inbox'));
    }

    public function update()
    {

        $id = $this->input->post('id');
        $find = $this->find($id);

        if($find) 
        {
            
            // GANTI INPUT SESUAI FORM
            $data = [
                'fullname' => $this->input->post('fullname'),            
                'email' => $this->input->post('email'),
                'subject'=> $this->input->post('subject'),
                'message'=> $this->input->post('message'),
            ];

            $this->db->where($this->pk, $id);
            $this->db->update($this->table, $data);

            $response = [
                'code' => 200,
                'status' => 'success',
                'message' => 'Berhasil memperbaharui'
            ];

        }
        else {

            $response = [
                'code' => 400,
                'status' => 'warning',
                'message' => 'Data tidak dapat ditemukan'
            ];

        }

        //GANTI REDIRECT SESUAI CONTROLLERNYA
        redirect(base_url('dashboard/inbox'));
    }

}

/* End of file Inbox.php */
