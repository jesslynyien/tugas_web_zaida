<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("_Partials/Head.php") ?>
</head>

<body>
  <!-- Preloader -->
  <?php $this->load->view("_Partials/Preloader.php") ?>

  <!-- Search Wrapper -->
  <?php $this->load->view("_Partials/SearchWrapper.php") ?>

  <!-- ##### Header Area Start ##### -->
  <?php $this->load->view("_Partials/HeaderArea.php") ?>
  <!-- ##### Header Area End ##### -->

  <!-- ##### Best Receipe Area Start ##### -->
  <section class="best-receipe-area">
    <div class="container" id="hasilpencarian">
      <div class="row">
        <div class="col-12">
          <div class="section-heading">
            <h3 id="JenisKuliner">Daftar Kuliner Minuman</h3>
          </div>
        </div>
      </div>

      <!-- <button class="btn delicious-btn mt-2" onclick="hide()">Hide Minuman</button> -->

      <div class="row">
        <!-- Single Best Receipe Area -->
        <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-best-receipe-area mb-30">
            <img src="<?php echo base_url('assets/img/bg-img/bandrek.jpg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Bandrek Susu</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"><i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 1969kcal </div>
                <div> : 166.49g</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 96.77g </div>
                <div> : 28.52g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 135 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 100 penjaja</div>
              </div>
            </div>
          </div>
          <div class="single-best-receipe-area mb-30">
            <img src="<?php echo base_url('assets/img/bg-img/sotoay.jpg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Soto ayam</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"> <i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 312kcal </div>
                <div> : 14,92g </div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 19,55g </div>
                <div> : 24,01g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 100 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 99 penjaja</div>
              </div>
            </div>
          </div>
          <!-- MieBakso -->
          <div class="single-best-receipe-area mb-30">
            <img src="<?php echo base_url('assets/img/bg-img/miebakso.jpg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Mie Bakso</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"><i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 1969kcal </div>
                <div> : 166.49g</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 96.77g </div>
                <div> : 28.52g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 135 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 100 penjaja</div>
              </div>
            </div>
          </div>
        </div>

        <!-- Single Best Receipe Area -->
        <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-best-receipe-area mb-30">
            <img src="<?php echo base_url('assets/img/bg-img/otak.jpeg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Otak - Otak </h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"> <i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 121 kcal </div>
                <div> : 3,3g</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 0g </div>
                <div> : 21,4g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 125 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 40 penjaja</div>
              </div>
            </div>
          </div>
          <div class="single-best-receipe-area mb-30">
            <img src="<?php echo base_url('assets/img/bg-img/pangsit.png') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Mie Pangsit</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"> <i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 1969kcal </div>
                <div> : 166.49g</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 96.77g </div>
                <div> : 28.52g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 135 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 100 penjaja</div>
              </div>
            </div>
          </div>
          <div class="single-best-receipe-area mb-30">
            <img class="gbrmini" src="<?php echo base_url('assets/img/bg-img/nasgor.jpg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Nasi Goreng</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"> <i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 333kcal </div>
                <div> : 12,34g</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 41,7g </div>
                <div> : 12,47g </div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 130 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fas fa-store"></i> 150 penjaja</div>
              </div>
            </div>
          </div>
        </div>

        <!-- Single Best Receipe Area -->
        <div class="col-12 col-sm-6 col-lg-4">

          <div class="single-best-receipe-area mb-30" id="myDIV">
            <img src="<?php echo base_url('assets/img/bg-img/escampur.jpg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Es Teler</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"> <i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 425kcal </div>
                <div> : 19,64gg</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 62,11g</div>
                <div> : 5.1g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 190 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 98 penjaja</div>
              </div>
            </div>
          </div>

          <div class="single-best-receipe-area mb-30">
            <img src="<?php echo base_url('assets/img/bg-img/buburay.jpeg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Bubur Ayam</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"><i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 1969kcal </div>
                <div> : 166.49g</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 96.77g </div>
                <div> : 28.52g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 135 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 100 penjaja</div>
              </div>
            </div>
          </div>
          <div class="single-best-receipe-area mb-30">
            <img src="<?php echo base_url('assets/img/bg-img/bihun_bebek.jpg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Bihun Bebek</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"><i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 1969kcal </div>
                <div> : 166.49g</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 96.77g </div>
                <div> : 28.52g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 135 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 100 penjaja</div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>
  <!-- ##### Best Receipe Area End ##### -->

  <!-- Page Navigation -->
  <div class="justify-content-center mb-5">
    <nav aria-label="Page navigation">
      <ul class="pagination justify-content-center">
        <li class="page-item disabled">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <nav aria-label="Page navigation example">
          <ul class="pagination">
            <li class="page-item active"><a class="page-link" href="#">01.</a></li>
            <li class="page-item"><a class="page-link" href="#">02.</a></li>
            <li class="page-item"><a class="page-link" href="#">03.</a></li>
          </ul>
        </nav>
        <li class="page-item active"><a class="page-link" href="#"></a></li>
        <li class="page-item"><a class="page-link" href="#"></a></li>
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul>
    </nav>
  </div>

  <section class="quote-subscribe-addsn mt-5">
    <div class="container">
      <div class="row align-items-end">
        <!-- ##### Follow Us Instagram Area Start ##### -->
        <?php $this->load->view("_Partials/GaleriEnd.php") ?>
        <!-- ##### Follow Us Instagram Area End ##### -->

        <!-- ##### Footer Area Start ##### -->
        <?php $this->load->view("_Partials/Footer.php") ?>
        <!-- ##### Footer Area Start ##### -->
      </div>
    </div>
  </section>

  <!-- ##### All Javascript Files ##### -->
  <?php $this->load->view("_Partials/Js.php") ?>
</body>

</html>

<script type="text/javascript">
  function hide() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }
</script>