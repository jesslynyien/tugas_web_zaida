<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("_Partials/Head.php") ?>
  <!-- rating.js file -->
  <script src="<?php echo base_url('assets/MDB-Free_4.11.0/js/addons/rating.min.js')?>"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.6/js/star-rating.min.js">
  </script>

</head>

<body>
  <!-- Preloader -->
  <?php $this->load->view("_Partials/Preloader.php") ?>

  <!-- Search Wrapper -->
  <?php $this->load->view("_Partials/SearchWrapper.php") ?>

  <!-- ##### Header Area Start ##### -->
  <?php $this->load->view("_Partials/HeaderArea.php") ?>
  <!-- ##### Header Area End ##### -->

  <!-- ##### Blog Area Start ##### -->
  <div class="blog-area section-padding-20">
    <div class="container">
      <div class="row">
        <div class="col-5 col-md-5">
          <!-- Gambar  -->
          <div class="container float-left">
            <div class="row">
              <div class="col-12">
                <div class="receipe-slider owl-carousel">
                  <img src="<?php echo base_url('assets/img/bg-img/sate_padang.jpg') ?>" alt="">
                </div>
              </div>
            </div>
          </div>

        </div>

        <!-- Side Bar  -->
        <div class="col-6 col-lg-6 ">
          <div class="row">
            <!-- Judul  -->
            <div class="receipe-headline my-5">
              <h2 class="mb-0" > Sate Padang Afrizal Amir</h2>
              
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2 mb-5"><i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>

              <hr>

              <div class="blog-sidebar-area ">
                <div class="row">
                  <div class="col-5 text-right">
                    <h6>Lokasi : </h6>
                  </div>
                  <div class="col-7">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.1422909153853!2d98.68624231387034!3d3.5546679515192814!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3031306e60f9e053%3A0x9a8eece4911bd599!2sSATE%20PADANG%20AFRIZAL%20AMIR!5e0!3m2!1sid!2sid!4v1576484713184!5m2!1sid!2sid" width="400" height="200" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                  </div>
                </div>
                <div class="row">
                  <div class="col-5 text-right">
                    <h6>Nomor Telepon : </h6>
                  </div>
                  <div class="col-7">
                  <h6>0822-7640-1761</h6>
                  </div>
                </div>
                <div class="row">
                  <div class="col-5 text-right">
                    <h6>Waktu Operasional : </h6>
                  </div>
                  <div class="col-7">
                    <h6>10.30 – 21.30 </h6>
                  </div>
                </div>

                <div class="row">
                  <div class="col-5 text-right">
                    <h6>Bisa dipesan melalui : </h6>
                  </div>
                  <div class="col-7">GrabFood & Go-Food</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <br>
  <br>

  <div class="container">
    <div class="col-12">
      <!-- ##### Comment List ##### -->
      <div class="col-12 col-lg-8 ">
        <div class="accordions mb-80" id="accordion" role="tablist" aria-multiselectable="true">

          <!-- Single Accordian Area -->
          <div class="panel single-accordion">
            <h6><a role="button" class="" aria-expanded="false" aria-controls="collapseOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Komentar (2)
                <span class="accor-open"><i class="fa fa-plus" aria-hidden="true"></i></span>
                <span class="accor-close"><i class="fa fa-minus" aria-hidden="true"></i></span>
              </a></h6>
            <div id="collapseOne" class="accordion-content collapse show">
              <div class="list-group">
                <ul class="list">
                  <li>

                    <div class="card text-green bg-light">
                      <img class="card-img-top" src="holder.js/100px180/" alt="">
                      <div class="card-body">
                        <h4 class="card-title">Anna </h4>
                        <div class="ratings">
                          <i class="fa fa-star" aria-hidden="true"></i>
                          <i class="fa fa-star" aria-hidden="true"></i>
                          <i class="fa fa-star" aria-hidden="true"></i>
                          <i class="fa fa-star" aria-hidden="true"></i>
                          <i class="fa fa-star-o" aria-hidden="true"></i>
                        </div>
                        <p class="card-text">Enak dan Hiegenis </p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="card text-black bg-light">
                      <img class="card-img-top" src="holder.js/100px180/" alt="">
                      <div class="card-body">
                        <h4 class="card-title">Rose </h4>
                        <div class="ratings">
                          <i class="fa fa-star" aria-hidden="true"></i>
                          <i class="fa fa-star" aria-hidden="true"></i>
                          <i class="fa fa-star" aria-hidden="true"></i>
                          <i class="fa fa-star" aria-hidden="true"></i>
                          <i class="fa fa-star-o" aria-hidden="true"></i>
                        </div>
                        <p class="card-text">Lezat, Pedas, Bikin Nagih... </p>
                      </div>
                    </div>
                  </li>
                  <li></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Leave a Comment -->
      <div class="section-heading text-left">
        <h3>Beri Rating dan Komentar</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
      <div class="ratings mb-15 ml-15" id="rateMe4" style="font-size : 20pt">
        <i class="fa fa-star" aria-hidden="true" id="s1" ></i>
        <i class="fa fa-star" aria-hidden="true" id="s2"></i>
        <i class="fa fa-star" aria-hidden="true" id="s3"></i>
        <i class="fa fa-star" aria-hidden="true" id="s4"></i>
        <i class="fa fa-star-o" aria-hidden="true" id="s5"></i>
      </div>
        <div class="contact-form-area mb-5">
          <form action="#" method="post">
            <div class="row">
              <div class="col-12">
                <textarea name="message" class="form-control" id="message" cols="30" rows="5" placeholder="Ketikkan Komentar Anda"></textarea>

              </div>
              <div class="col-12">
                <button id="comment" onclick="komentar()" class="btn delicious-btn mt-30" >Kirim Masukan</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  </div>

  <!-- ##### Blog Area End ##### -->

  <section class="quote-subscribe-addsn mt-5">
    <div class="container">
      <div class="row align-items-end">
        <!-- ##### Follow Us Instagram Area Start ##### -->
        <?php $this->load->view("_Partials/GaleriEnd.php") ?>
        <!-- ##### Follow Us Instagram Area End ##### -->

        <!-- ##### Footer Area Start ##### -->
        <?php $this->load->view("_Partials/Footer.php") ?>
        <!-- ##### Footer Area Start ##### -->
      </div>
    </div>
  </section>

  <!-- ##### All Javascript Files ##### -->
  <?php $this->load->view("_Partials/Js.php") ?>
</body>

</html>

<script type="text/javascript">

  function komentar(){
    alert('Masukan dari Anda sangat bermanfaat bagi kami, Terima Kasih !!')
  }

  
</script>

 