    <!-- ##### Galeri Area Start ##### -->
    <div class="follow-us-instagram">
        <div class="container">
        <div class="row">
            <div class="col-12">
            <h5>Galeri Kuliner</h5>
            </div>
        </div>
        </div>
        <!-- Galeri Feeds -->
        <div class="insta-feeds d-flex flex-wrap">
        <!-- Single Insta Feeds -->
        <div class="single-insta-feeds">
            <img src="<?php echo base_url('assets/img/bg-img/escampur.jpg') ?>" alt="">
            <!-- Icon -->
            <div class="insta-icon">
            <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
            </div>
        </div>
        <!-- Single Insta Feeds -->
        <div class="single-insta-feeds">
            <img src="<?php echo base_url('assets/img/bg-img/martabak.jpg') ?>" alt="">
            <!-- Icon -->
            <div class="insta-icon">
            <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
            </div>
        </div>
        <!-- Single Insta Feeds -->
        <div class="single-insta-feeds">
            <img src="<?php echo base_url('assets/img/bg-img/buburay.jpeg') ?>" alt="">
            <!-- Icon -->
            <div class="insta-icon">
            <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
            </div>
        </div>
        <!-- Single Insta Feeds -->
        <div class="single-insta-feeds">
            <img src="<?php echo base_url('assets/img/bg-img/paris.jpg  ') ?>" alt="">
            <!-- Icon -->
            <div class="insta-icon">
            <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
            </div>
        </div>
        <!-- Single Insta Feeds -->
        <div class="single-insta-feeds">
            <img src="<?php echo base_url('assets/img/bg-img/nasgor.jpg') ?>" alt="">
            <!-- Icon -->
            <div class="insta-icon">
            <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
            </div>
        </div>
        <!-- Single Insta Feeds -->
        <div class="single-insta-feeds">
            <img src="<?php echo base_url('assets/img/bg-img/miebakso.jpg') ?>" alt="">
            <!-- Icon -->
            <div class="insta-icon">
            <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
            </div>
        </div>
        <!-- Single Insta Feeds -->
        <div class="single-insta-feeds">
            <img src="<?php echo base_url('assets/img/bg-img/sate_padang.jpg') ?>" alt="">
            <!-- Icon -->
            <div class="insta-icon">
            <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
            </div>
        </div>


        </div>
    </div>
    <!-- ##### Galeri Area End ##### -->
    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-12 h-100 d-flex flex-wrap align-items-center justify-content-between">
                    <!-- Footer Social Info -->
                    <div class="footer-social-info text-right">
                        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    </div>
                    <!-- Footer Logo -->
                    <div class="footer-logo">
                        <a href="index.html"><img src="<?php echo base_url('assets/img/core-img/logo.png') ?>" alt=""></a>
                    </div>
                    <!-- Copywrite -->
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;
                        <script>document.write(new Date().getFullYear());</script> All rights reserved | This template
                        is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com"
                            target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area Start ##### -->



    <!-- jQuery-2.2.4 js -->
    <script src="<?php echo base_url('assets/js/jquery/jquery-2.2.4.min.js')?>"></script>
    <!-- Popper js -->
    <script src="<?php echo base_url('assets/js/bootstrap/popper.min.js')?>"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url('assets/js/bootstrap/bootstrap.min.js')?>"></script>
    <!-- All Plugins js -->
    <script src="<?php echo base_url('assets/js/plugins/plugins.js')?>"></script>
    <!-- Active js -->
    <script src="<?php echo base_url('assets/js/active.js')?>"></script>
</body>

</html>