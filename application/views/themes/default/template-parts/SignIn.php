    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="breadcrumb_iner">
              <div class="breadcrumb_iner_item">
                <p>Sign in to start</p>
                <h2>Sign In</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- breadcrumb start-->

    <!-- Form Element -->
    <div class="whole-wrap">
      <div class="container box-1170">
        <div class="section-top-border">
          <div class="row">
            <div class="col-12 col-lg-6 col-md-6 mr-10 mt-20">
              <div class="contact-form-area">
                <form action="<?php echo base_url('welcome/check_signin') ?>" method="POST">
                  <div class="mt-10">
                      <input type="username" name="username" placeholder="Username" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'" required class="single-input form-control">
                  </div>
                  <div class="mt-10">
                      <input type="password" name="password" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'" required class="single-input form-control">
                  </div>
                  <div class="form-group mt-10">
                    <button type="submit" class="btn delicious-btn">Sign In</button>
                  </div>

                </form>
              </div>
            </div>
        </div>
        </div> 
      </div>
      
    </div>
  