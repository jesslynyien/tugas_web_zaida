<!-- ##### Breadcumb Area Start ##### -->
<div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?php echo base_url('assets/img/bg-img/breadcumb2.jpg') ?>);">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcumb-text text-center">
            <h2>Pencarian</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ##### Breadcumb Area End ##### -->

  <!-- ##### Blog Area Start ##### -->
  <div class="blog-area section-padding-80">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-12">
          <div class="blog-posts-area">

            <?php 
            

                $posts = $this->db->select()->from('post')
                    ->where('post_type', $this->input->get('post_type'))
                    ->like('post_title', $this->input->get('s'))
                    ->get();

            ?>
            
            <?php if($posts->num_rows()) : ?>

                <?php foreach($posts->result() as $post) : ?>
                    <?php $date = new DateTime($post->post_date); ?>
                    <!-- Single Blog Area -->
                    <div class="single-blog-area mb-80">
                        <!-- Thumbnail -->
                        <div class="blog-thumbnail">
                            <?php $thumbnail = !empty($post->post_thumbnail) ? base_url('uploads/' . $post->post_thumbnail) : 'https://via.placeholder.com/728x300.png?text=Placeholder' ?>
                            <img src="<?php echo $thumbnail; ?>" alt="Lihat <?php echo $post->post_title; ?>" class="w-100">
                            <!-- Post Date -->
                            <div class="post-date">
                            <a href="#"><span><?php echo $date->format('d') ?></span><?php echo $date->format('F'); ?> <br> <?php echo $date->format('Y') ?></a>
                            </div>
                        </div>
                        <!-- Content -->
                        <div class="blog-content">
                            <a href="<?php echo base_url('kuliner/' . $post->post_slug) ?>" class="post-title"><?php echo $post->post_title; ?></a>
                            <!-- <div class="meta-data">by <a href="javascript:void(0)">Maria Williams</a></div> -->
                            
                            <p><?php echo substr(strip_tags($post->post_content), 0, 200) ?></p>
                            <a href="<?php echo base_url('kuliner/' . $post->post_slug) ?>" class="btn delicious-btn mt-30">Read More</a>
                        </div>
                    </div>
                <?php endforeach; ?>

            <?php endif; ?>
       

          </div>

          <!-- <nav aria-label="Page navigation example">
            <ul class="pagination">
              <li class="page-item active"><a class="page-link" href="#">01.</a></li>
              <li class="page-item"><a class="page-link" href="#">02.</a></li>
              <li class="page-item"><a class="page-link" href="#">03.</a></li>
            </ul>
          </nav> -->
        </div>

      </div>
    </div>
  </div>
  <!-- ##### Blog Area End ##### -->