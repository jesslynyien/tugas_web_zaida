
<?php 

    $posts = $this->db->select()->from('post')
        ->where('post_type', 'post')
        ->where('post_slug', $slug)
        ->get();

?>
<?php if($posts->num_rows()) : ?>

<?php foreach($posts->result() as $post) : ?>
<!-- ##### Breadcumb Area Start ##### -->
<?php $thumbnail = !empty($post->post_thumbnail) ? base_url('uploads/' . $post->post_thumbnail) : 'https://via.placeholder.com/728x300.png?text=Placeholder' ?>
                            
<div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?php echo $thumbnail; ?>);">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcumb-text text-center">
            <h2><?php echo $post->post_title; ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ##### Breadcumb Area End ##### -->

  <!-- ##### Blog Area Start ##### -->
  <div class="blog-area section-padding-80">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-12">
          <div class="blog-posts-area">

            
   
                    <?php $date = new DateTime($post->post_date); ?>
                    <!-- Single Blog Area -->
                    <div class="single-blog-area mb-80">
                        <!-- Thumbnail -->
                        <!-- <div class="blog-thumbnail">
                            <img src="<?php echo $thumbnail; ?>" alt="Lihat <?php echo $post->post_title; ?>" class="w-100">
                            Post Date
                            <div class="post-date">
                            <a href="#"><span><?php echo $date->format('d') ?></span><?php echo $date->format('F'); ?> <br> <?php echo $date->format('Y') ?></a>
                            </div>
                        </div> -->
                        <!-- Content -->
                        <div class="blog-content">
                        
                            <div><?php echo $post->post_content; ?></div>
                         
                            
                        </div>
                    </div>
                <?php endforeach; ?>

            <?php endif; ?>
       

          </div>

          <!-- <nav aria-label="Page navigation example">
            <ul class="pagination">
              <li class="page-item active"><a class="page-link" href="#">01.</a></li>
              <li class="page-item"><a class="page-link" href="#">02.</a></li>
              <li class="page-item"><a class="page-link" href="#">03.</a></li>
            </ul>
          </nav> -->
        </div>

      </div>
    </div>
  </div>
  <!-- ##### Blog Area End ##### -->