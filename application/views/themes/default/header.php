<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>INI KEDAN | Kuliner Medan</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url('assets/style.css') ?>">

</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <i class="circle-preloader"></i>
        <img src="<?php echo base_url('assets/img/core-img/salad.png') ?>" alt="">
    </div>

    <!-- Search Wrapper -->
    <div class="search-wrapper">
        <!-- Close Btn -->
        <div class="close-btn"><i class="fa fa-times" aria-hidden="true"></i></div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form action="<?php echo base_url('/search') ?>" method="get">
                        <input type="hidden" name="post_type" value="kuliner">
                        <input type="search" name="s" placeholder="Type any keywords...">
                        <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- Top Header Area -->
        <div class="top-header-area">
            <div class="container h-100">
            <div class="row h-100 align-items-center justify-content-between">
                <!-- Breaking News -->
                <div class="col-12 col-sm-6">
                    <div class="breaking-news">
                        <!-- SignIn -->
                        <!-- Button trigger modal -->

                        <?php $is_login = $this->session->userdata('is_login'); ?>


                        <?php if(!$is_login) : ?>
                        <a href="<?php echo base_url('signin') ?>" class="mr-2">Sign in</a>
                        <a href="<?php echo base_url('signup') ?>">Sign up</a>
                        <?php else: ?>
                            
                        <a href="<?php echo base_url('welcome/logout') ?>">Logout</a>

                        <?php endif; ?>

                
                    </div>
                </div>

                <!-- Top Social Info -->
                <div class="col-12 col-sm-6">
                <div class="top-social-info text-right">
                    <div class="login-btn">
                    </div>
                    <div class="search-btn">
                    <i class="fa fa-search" aria-hidden="true"></i>
                    </div>
                </div>
                </div>

            </div>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="delicious-main-menu">
            <div class="classy-nav-container breakpoint-off">
            <div class="container">
                <!-- Menu -->
                <nav class="classy-navbar justify-content-between" id="deliciousNav">

                <!-- Logo -->
                <a class="nav-brand" href="<?php echo base_url('/') ?>"><img src="<?php echo base_url('assets/img/core-img/nvidia.png') ?>" alt=""></a>
                <!-- Navbar Toggler -->
                <div class="classy-navbar-toggler">
                    <span class="navbarToggler"><span></span><span></span><span></span></span>
                </div>

                <!-- Menu -->
                <div class="classy-menu">

                    <!-- close btn -->
                    <div class="classycloseIcon">
                    <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                    </div>

                    <!-- Nav Start -->
                    <div id="navigasi justify-content-center" class="classynav">
                    <ul>
                        <li class="nav" id="homeLink"><a href="<?php echo base_url('/') ?>">Beranda</a></li>
                        <li class="nav" id="blogLink"><a href="<?php echo base_url('post') ?>">Artikel</a></li>
                        <!-- ini juga? oh i see -->
                        <li id="categoryLink"><a href="#">Kategori</a>
                        <ul class="dropdown">
                            <li><a onclick="semua()" href="<?php echo base_url('list') ?>">Semua</a></li>
                            <li><a href="#">Jenis</a>
                            <ul class="dropdown">
                                <li><a onclick="makanan()">Makanan</a></li>
                                <li><a onclick="minuman()">Minuman</a></li>
                            </ul>
                            </li>

                            <!-- Ini apa ? category ? ya -->
                            <li><a href="#">Fitur</a>
                            <ul class="dropdown">
                                <li><a onclick="halal()" >Halal</a></li>
                                <li><a onclick="nonhalal()">Non-Halal</a></li>
                                <li><a onclick="vege()">Vegetarian</a></li>
                            </ul>
                            </li>
                        </ul>
                        </li>
                        <li class="nav" id="AskLink"><a href="<?php echo base_url('ask'); ?>">Tanya Ahli Gizi</a></li>
                        </li>
                        <li class="nav" id="FavLink"><a href="<?php echo base_url('penjual'); ?>">Daftar Favorit</a></li>
                        </li>
                        <li class="nav" id="FavLink"><a href="<?php echo base_url('promo'); ?>">Promo</a></li>
                        </li>
                        <li class="nav" id="contactLink"><a href="<?php echo base_url('contact'); ?>">Hubungi Kami</a></li>
                        </li>
                        <li>
                        <div class="search-btn">
                        </div>
                        </li>
                    </ul>
                    </div>
                    <!-- Nav End -->
                </div>
                </nav>
            </div>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->