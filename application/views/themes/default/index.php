  <!-- ##### Top Catagory Area Start ##### -->
  <section class="hero-area mb-5">
    <div class="hero-slides owl-carousel">
      <div class="row">
        <!-- Top Catagory Area -->
        <div class="col-12 col-lg-4">
          <div class="single-top-catagory">
            <img src="<?php echo base_url('assets/img/bg-img/bg3 - Copy.jpg') ?>" alt="">
            <!-- Content -->
            <div class="top-cta-content">
              <h3>Cocok untuk Pertumbuhan</h3>
              <h6>Kaya Protein &amp; Vitamin</h6>
              <a href="<?php echo base_url('index.php/listkuliner') ?>" class="btn delicious-btn">Lihat Daftar</a>
            </div>
          </div>
        </div>
        <!-- Top Catagory Area -->
        <div class="col-12 col-lg-4">
          <div class="single-top-catagory">
            <img src="<?php echo base_url('assets/img/bg-img/bumil.jpg') ?>" alt="">
            <!-- Content -->
            <div class="top-cta-content">
              <h3>Penuhi Gizi Ibu Hamil</h3>
              <h6>Kaya Vitamin &amp; Sehat bagi janin</h6>
              <a href="<?php echo base_url('index.php/listkuliner') ?>" class="btn delicious-btn">Lihat Daftar</a>
            </div>
          </div>
        </div>
        <!-- Top Catagory Area -->
        <div class="col-12 col-lg-4">
          <div class="single-top-catagory">
            <img src="<?php echo base_url('assets/img/bg-img/bubur.jpg') ?>" alt="">
            <!-- Content -->
            <div class="top-cta-content">
              <h3>Aman bagi Lansia</h3>
              <h6>Rendah Gula &amp; Bebas Kolesterol</h6>
              <a href="<?php echo base_url('index.php/listkuliner') ?>" class="btn delicious-btn">Lihat Daftar</a>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>
  <!-- ##### Top Catagory Area End ##### -->

  <!-- ##### Hero Area Start ##### -->
  <section class="hero-area top-catagory-area section-padding-80-20 mb-5">
    <div class="hero-slides owl-carousel ">

      <?php 
      

        $sliders = $this->db->select()->from('post_taxonomy')
          ->join('post', 'post.id_post=post_taxonomy.id_post', 'left')
          ->join('categories', 'categories.id_cat=post_taxonomy.id_cat','left')
          ->where('categories.cat_slug', 'homepage-slider') // DARI MENU  http://tugas_web_zaida.test/dashboard/category?taxonomy=slider
          ->where('post_type', 'slider')->get();

        
      
      ?>

      <?php if($sliders->num_rows() > 0) : ?>
        <?php foreach($sliders->result() as $slider) : ?>

          <?php $post_thumbnail = ($slider->post_thumbnail) ? base_url('uploads/' . $slider->post_thumbnail) : ""; ?>
          <!-- Single Hero Slide -->
          <div class="single-hero-slide bg-img" style="background-image: url(<?php echo $post_thumbnail; ?>);">
            <div class="container h-100">
              <div class="row h-100 align-items-center">
                <div class="col-12 col-md-9 col-lg-7 col-xl-6">
                  <div class="hero-slides-content" data-animation="fadeInUp" data-delay="100ms">
                    <h2 data-animation="fadeInUp" data-delay="300ms"><?php echo $slider->post_title; ?></h2>
                    <div data-animation="fadeInUp" data-delay="700ms"> 

                      <?php echo $slider->post_content; ?>

                    </div>
                    <?php 
                    

                    $btn_cta_name = $this->form->get_meta($slider->id_post, 'btn_cta_name');
                    $btn_cta_url = $this->form->get_meta($slider->id_post, 'btn_cta_url');
                    $btn_cta_target = $this->form->get_meta($slider->id_post, 'btn_cta_target');
                    
                    
                    ?>
                    <?php if($btn_cta_name) : ?>
                      <a href="<?php echo $btn_cta_url; ?>" <?php if($btn_cta_name) echo "target='{$btn_cta_target}'"; ?> class="btn delicious-btn" data-animation="fadeInUp" data-delay="1000ms"><?php echo $btn_cta_name; ?></a>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endforeach; ?>
      <?php endif; ?>

     
    </div>
  </section>
  <!-- ##### Hero Area End ##### -->

  <!-- ##### Best Receipe Area Start ##### -->
  <section class="best-receipe-area mt-30">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="section-heading">
            <h3>Kuliner Medan Populer</h3>
          </div>
        </div>
      </div>

      <?php 
      
      $foods = $this->db->select()->from('post')
          ->where('post_type', 'kuliner')
          ->get();

      
      ?>

    <div class="row">

      <?php if($foods->num_rows() > 0 ) : ?>

        <?php foreach($foods->result() as $food) : ?>

          <?php 
          
          $post_thumbnail = $food->post_thumbnail ? base_url('uploads/' . $food->post_thumbnail) : "https://via.placeholder.com/350x250.png?text=Placeholder";
          $kalori = (int)$this->form->get_meta($food->id_post, 'kalori');
          $lemak = (int)$this->form->get_meta($food->id_post, 'lemak');
          $karbohidrat = (int)$this->form->get_meta($food->id_post, 'karbohidrat');
          $protein = (int)$this->form->get_meta($food->id_post, 'protein');
          $rating = (int)$this->form->get_meta($food->id_post, 'rating');
            
          ?>
          <div class="col-12 col-sm-6 col-lg-4">
            <div class="single-best-receipe-area mb-30">
              <img src="<?php echo $post_thumbnail; ?>" alt="">
              <div class="receipe-content">
                <a href="<?php echo base_url('kuliner/' . $food->post_slug) ?>">
                  <h5><?php echo $food->post_title ?></h5>
                </a>


                <div class="ratings">
                  <?php 

                    $max = 5;
                    $sisa = $max-$rating;
                  
                  ?>

                  <?php for($i = 0; $i < $rating; $i++) : ?>
                    <i class="fa fa-star" aria-hidden="true"></i>
                  <?php endfor; ?>

                  <?php for($i = 0; $i < $sisa; $i++) : ?>
                    <i class="fa fa-star-o" aria-hidden="true"></i>
                  <?php endfor; ?>
                </div>
                <a href="<?php echo base_url('penjual') ?>" class="btn delicious-btn mt-2"><i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
              </div>
              <div class="row mt-2">
                <div class="col-sm-2 text-left">
                  <div> Kal </div>
                  <div> Lemak </div>
                </div>
                <div class="col-sm-4 text-left">
                  <div> : <?php echo $kalori; ?>cal </div>
                  <div> : <?php echo $lemak; ?>g</div>
                </div>
                <div class="col-sm-2 text-right">
                  <div> Karbo </div>
                  <div> Protein</div>
                </div>
                <div class="col-sm-4 text-right">
                  <div> : <?php echo $karbohidrat; ?>g </div>
                  <div> : <?php echo $protein; ?>g</div>
                </div>
              </div>
              <hr>
              <div class="row mb-3">
                <div class="col-sm-5 text-center">
                  <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 135 orang </div>
                </div>
                <div class="col-sm-6 text-center">
                  <div class="country"> <i class="fa fa-store"></i> 100 penjaja</div>
                </div>
              </div>
            </div>
          </div>

        <?php endforeach; ?>

      <?php endif; ?>

      
        <!-- Single Best Receipe Area -->
        <!-- <div class="col-12 col-sm-6 col-lg-4">

          <div class="single-best-receipe-area mb-30">
            <img src="<?php echo base_url('assets/img/bg-img/sate_padang.jpg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Sate Padang</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"><i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 1969kcal </div>
                <div> : 166.49g</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 96.77g </div>
                <div> : 28.52g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 135 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 100 penjaja</div>
              </div>
            </div>
          </div>

          <div class="single-best-receipe-area mb-30">
            <img src="<?php echo base_url('assets/img/bg-img/sotoay.jpg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Soto ayam</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"> <i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 312kcal </div>
                <div> : 14,92g </div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 19,55g </div>
                <div> : 24,01g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 100 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 99 penjaja</div>
              </div>
            </div>
          </div>
          <div class="single-best-receipe-area mb-30">
            <img src="<?php echo base_url('assets/img/bg-img/escampur.jpg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Es Teler</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"> <i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 425kcal </div>
                <div> : 19,64gg</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 62,11g</div>
                <div> : 5.1g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 190 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 98 penjaja</div>
              </div>
            </div>
          </div>
        </div> -->

        <!-- Single Best Receipe Area -->
        <!-- <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-best-receipe-area mb-30">
            <img src="<?php echo base_url('assets/img/bg-img/otak.jpeg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Otak - Otak </h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"> <i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 121 kcal </div>
                <div> : 3,3g</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 0g </div>
                <div> : 21,4g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 125 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 40 penjaja</div>
              </div>
            </div>
          </div>
          <div class="single-best-receipe-area mb-30">
            <img src="<?php echo base_url('assets/img/bg-img/pangsit.png') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Mie Pangsit</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"> <i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 1969kcal </div>
                <div> : 166.49g</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 96.77g </div>
                <div> : 28.52g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 135 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 100 penjaja</div>
              </div>
            </div>
          </div>
          <div class="single-best-receipe-area mb-30">
            <img class="gbrmini" src="<?php echo base_url('assets/img/bg-img/nasgor.jpg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Nasi Goreng</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"> <i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 333kcal </div>
                <div> : 12,34g</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 41,7g </div>
                <div> : 12,47g </div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 130 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fas fa-store"></i> 150 penjaja</div>
              </div>

            </div>
          </div>
        </div> -->

        <!-- Single Best Receipe Area -->
        <!-- <div class="col-12 col-sm-6 col-lg-4">
          <div class="single-best-receipe-area mb-30">
            <img src="<?php echo base_url('assets/img/bg-img/miebakso.jpg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Mie Bakso</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"><i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 1969kcal </div>
                <div> : 166.49g</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 96.77g </div>
                <div> : 28.52g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 135 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 100 penjaja</div>
              </div>
            </div>
          </div>

          <div class="single-best-receipe-area mb-30">
            <img src="<?php echo base_url('assets/img/bg-img/buburay.jpeg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Bubur Ayam</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"><i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 1969kcal </div>
                <div> : 166.49g</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 96.77g </div>
                <div> : 28.52g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 135 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 100 penjaja</div>
              </div>
            </div>
          </div>
          <div class="single-best-receipe-area mb-30">
            <img src="<?php echo base_url('assets/img/bg-img/bihun_bebek.jpg') ?>" alt="">
            <div class="receipe-content">
              <a href="<?php echo base_url('index.php/detailgizi') ?>">
                <h5>Bihun Bebek</h5>
              </a>
              <div class="ratings">
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
              </div>
              <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"><i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>
            </div>
            <div class="row mt-2">
              <div class="col-sm-2 text-left">
                <div> Kal </div>
                <div> Lemak </div>
              </div>
              <div class="col-sm-4 text-left">
                <div> : 1969kcal </div>
                <div> : 166.49g</div>
              </div>
              <div class="col-sm-2 text-right">
                <div> Karbo </div>
                <div> Protein</div>
              </div>
              <div class="col-sm-4 text-right">
                <div> : 96.77g </div>
                <div> : 28.52g</div>
              </div>
            </div>
            <hr>
            <div class="row mb-3">
              <div class="col-sm-5 text-center">
                <div class="country"> <i class="fa fa-eye" aria-hidden="true"></i> 135 orang </div>
              </div>
              <div class="col-sm-6 text-center">
                <div class="country"> <i class="fa fa-store"></i> 100 penjaja</div>
              </div>
            </div>
          </div>
        </div> -->

      </div>
    </div>
  </section>
  <!-- ##### Best Receipe Area End ##### -->


  <!-- ##### CTA Area Start ##### -->
  <section class="cta-area bg-img bg-overlay" style="background-image: url(<?= base_url() ?>assets/img/bg-img/bg4.jpg);">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <!-- Cta Content -->
          <div class="cta-content text-center">
            <h2>Info news</h2>
            <p>Baca lebih tips lengkap tentang hidup sehat & rekomendasi kuliner terbaru</p>
            <a href="#" class="btn delicious-btn">Discover all the receipies</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ##### CTA Area End ##### -->


  <!-- ##### Quote Subscribe Area Start ##### -->
  <section class="quote-subscribe-addsn mt-15">
    <div class="container">
      <div class="row align-items-end">
        <!-- Quote -->
        <div class="col-12 col-lg-12">
          <div class="quote-area text-center">
            <h4>"Tidak ada yang lebih baik daripada pulang ke rumah untuk keluarga, makan makanan enak dan dengan santai"</h4>
            <p>John Smith</p>
            <div class="date-comments d-flex justify-content-between">
              <div class="date">January 04, 2018</div>
              <div class="comments">2 Comments</div>
            </div>
          </div>
        </div>
        <!-- ##### Quote Subscribe Area End ##### -->

        <!-- ##### Galeri Area Start ##### -->
        <?php //$this->load->view("_Partials/GaleriEnd.php") ?>
        <!-- ##### Galeri Area End ##### -->

        <!-- ##### Footer Area Start ##### -->
        <?php //$this->load->view("_Partials/Footer.php") ?>
        <!-- ##### Footer Area Start ##### -->
      </div>
    </div>
  </section>

