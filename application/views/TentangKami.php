<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("_Partials/Head.php") ?>
</head>

<body>
  <!-- Preloader -->
  <?php $this->load->view("_Partials/Preloader.php") ?>

  <!-- Search Wrapper -->
  <?php $this->load->view("_Partials/SearchWrapper.php") ?>

  <!-- ##### Header Area Start ##### -->
  <?php $this->load->view("_Partials/HeaderArea.php") ?>
  <!-- ##### Header Area End ##### -->

  <!-- ##### Breadcumb Area Start ##### -->
  <div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?= base_url() ?>assets/img/bg-img/miebakso.jpg);">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcumb-text text-center">
            <h2>Tentang Kami</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ##### Breadcumb Area End ##### -->

  <!-- ##### About Area Start ##### -->
  <section class="about-area section-padding-80">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="section-heading">
            <h3 style="color: green;">APLIKASI INI KEDAN</h3>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <h6 class="sub-heading pb-5">INI KEDAN ( Informasi Gizi Kuliner Medan) merupakan sebuah aplikasi web yang bertujuan untuk menampilkan informasi gizi kuliner Medan sekaligus menawarkan tempat-tempat menarik yang menjual masing masing kuliner itu. Kata KEDAN di Medan dikenal luas dengan arti "Teman Akrab / Kawan Baik" yang berarti aplikasi INI KEDAN bisa menjadi teman baik para pengguna untuk bisa hidup sehat.</h6>
          <h6 class="sub-heading pb-5">Ayo Segera Temukan Fakta Menarik dibalik kuliner Medan yang lezaat ini !!!</h6>

          <p class="text-center mb-10 font-weight-bold">Medan merupakan salah satu kota kuliner terbaik di Indonesia (SumutCyber.com, 2019). Sayangnya, pengetahuan masyarakat mengenai ragam kuliner Kota Medan apalagi nilai gizi yang terkandung di dalamnya masih rendah. Hal ini dibuktikan dari angka prevalensi stunting (gizi buruk) balita Indonesia yang berada di angka 30,8%, turun 6,4% dari tahun 2013 (Kemenkes, 2018). Hasil Riskesdas 2018 juga menyebutkan kondisi konsumsi makanan ibu hamil dan balita tahun 2016-2017 menunjukkan di Indonesia 1 dari 5 ibu hamil kurang gizi, 7 dari 10 ibu hamil kurang kalori dan protein, 7 dari 10 balita kurang kalori, serta 5 dari 10 balita kurang protein (Kemenkes, 2018). Jadi, gizi sangat penting, terutama untuk orang dengan keadaan khusus, yaitu : balita, ibu hamil, hingga lansia. Menurut Prof. Astawan, penyebab utamanya adalah faktor ekonomi dan pendidikan gizi. Tidak sedikit masyarakat yang berkecukupan secara finansial memiliki pengetahuan gizi rendah, begitupun masyarakat yang memang kurang secara finansial, gizinya pun kurang (Kompas, 2013). Dalam konsep gizi, tak ada satupun makanan yang bisa mencakup semua gizi yang diperlukan tubuh. Sifat dari bahan pangan mengandung kelebihan dan kekurangan, ada yang tinggi dan rendah, kecuali air susu ibu untuk bayi di bawah 6 bulan (ZywieLab, 2016). </p>
          <br>
          <br>

          <!-- FITUR  -->
          <div class="row">
            <div class="col-12 col-lg-8">
              <div class="section-heading">
                <h3 style="color: green;">FITUR MENARIK INI KEDAN</h3>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-lg-8">
              <!-- Single Preparation Step -->
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">01. </h5>
                <h6> Menyediakan informasi tentang kandungan gizi dari kuliner Kota Medan </h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">02. </h5>
                <h6> Menampilkan perbandingan gizi kuliner dengan AKG (Angka Kecukupan Gizi) pengguna berdasarkan usia dan jenis kelamin </h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">03. </h5>
                <h6> Memberikan peringatan bagi kuliner yang tidak cocok dengan ibu hamil, anak-anak, ataupun lansia</h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">04. </h5>
                <h6> Menyediakan detail penjual kuliner beserta navigasi menuju lokasi kuliner</h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">05. </h5>
                <h6> Menyediakan artikel tentang fakta menarik seputar pentingnya gizi dan hidup sehat </h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">06. </h5>
                <h6> Menyediakan halaman untuk tanya jawab dengan dokter gizi</h6>
              </div>
            </div>
          </div>

          <br>
          <br>

          <!-- KEGUNAAN -->
          <div class="row">
          <div class="col-12 col-lg-4"></div>
            <div class="col-12 col-lg-8">
              <div class="section-heading">
                <h3 style="color: green;">KEGUNAAN APLIKASI INI KEDAN</h3>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-lg-4"></div>
            <div class="col-12 col-lg-8" >
              <!-- Single Preparation Step -->
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">01. </h5>
                <h6> Meningkatkan kesadaran masyarakat khususnya Kota Medan dan sekitarnya tentang pentingnya pemenuhan dan keseimbangan gizi </h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">02. </h5>
                <h6> Menyediakan informasi kandungan nilai gizi dari kuliner Medan. </h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">03. </h5>
                <h6>Memberikan informasi mengenai letak penjual kuliner Medan berdasarkan kategori makanan yang diinginkan.</h6>
              </div>
              <div class="single-preparation-step d-flex mb-3">
                <h5 class="mr-4" style="color: green;">04. </h5>
                <h6>Memberi infomasi mengenai fakta seputar gizi.</h6>
              </div>
              <h6> Dengan adanya aplikasi web ini diharapkan pengetahuan dan kepedulian masyarakat Indonesia akan gizi bertambah sehingga masalah gizi buruk balita, kekurangan gizi pada ibu hamil di Kota Medan dan sekitarnya bisa diminimalisir. </h6>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ##### About Area End ##### -->

  <section class="quote-subscribe-addsn mt-5">
    <div class="container">
      <div class="row align-items-end">
        <!-- ##### Follow Us Instagram Area Start ##### -->
        <?php $this->load->view("_Partials/GaleriEnd.php") ?>
        <!-- ##### Follow Us Instagram Area End ##### -->

        <!-- ##### Footer Area Start ##### -->
        <?php $this->load->view("_Partials/Footer.php") ?>
        <!-- ##### Footer Area Start ##### -->
      </div>
    </div>
  </section>

  <!-- ##### All Javascript Files ##### -->
  <?php $this->load->view("_Partials/Js.php") ?>
</body>

</html>