<header class="header-area">

  <!-- Top Header Area -->
  <div class="top-header-area">
    <div class="container h-100">
      <div class="row h-100 align-items-center justify-content-between">
        <!-- Breaking News -->
        <div class="col-12 col-sm-6">
          <div class="breaking-news">
            <!-- SignIn -->
            <!-- Button trigger modal -->
            <a href="#" type="button" class="btn btn-light" data-toggle="modal" data-target="#SignIn">Masuk</a>

            <!-- Modal -->
            <div class="modal fade" id="SignIn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Daftar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form>
                      <div class="form-group">
                        <label for="inputEmail4">Email</label>
                        <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                      </div>
                      <div class="form-group">
                        <label for="inputPassword4">Password</label>
                        <input type="password" class="form-control" id="inputPassword4" placeholder="Masukkan Password">
                      </div>
                      <button type="submit" class="btn btn-primary">Masuk</button>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                  </div>
                </div>
              </div>
            </div>

            <!-- SignUp  -->
            <!-- Button trigger modal -->
            <a href="#" type="button" class="btn btn-light" data-toggle="modal" data-target="#SignUp">Daftar</a>

            <!-- Modal -->
            <div class="modal fade" id="SignUp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Daftar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form>
                      <div class="form-group">
                        <label for="inputEmail4">Email</label>
                        <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
                      </div>
                      <div class="form-group">
                        <label for="inputPassword4">Password</label>
                        <input type="password" class="form-control" id="inputPassword4" placeholder="Masukkan Password">
                      </div>
                      <div class="form-group">
                        <label for="inputPassword4">Ulangi Password</label>
                        <input type="password" class="form-control" id="inputPassword4" placeholder="Ulangi Password">
                      </div>
                      <div class="form-group">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" id="gridCheck">
                          <label class="form-check-label" for="gridCheck">
                            Saya setuju dengan <a href="" style="text-decoration: underline"> S&K yang berlaku</a>
                          </label>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-primary">Daftarkan Saya</button>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Top Social Info -->
        <div class="col-12 col-sm-6">
          <div class="top-social-info text-right">
            <div class="login-btn">
            </div>
            <div class="search-btn">
              <i class="fa fa-search" aria-hidden="true"></i>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- Navbar Area -->
  <div class="delicious-main-menu">
    <div class="classy-nav-container breakpoint-off">
      <div class="container">
        <!-- Menu -->
        <nav class="classy-navbar justify-content-between" id="deliciousNav">

          <!-- Logo -->
          <a class="nav-brand" href="<?php echo base_url('/') ?>"><img src="<?php echo base_url('assets/img/core-img/nvidia.png') ?>" alt=""></a>
          <!-- Navbar Toggler -->
          <div class="classy-navbar-toggler">
            <span class="navbarToggler"><span></span><span></span><span></span></span>
          </div>

          <!-- Menu -->
          <div class="classy-menu">

            <!-- close btn -->
            <div class="classycloseIcon">
              <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
            </div>

            <!-- Nav Start -->
            <div id="navigasi justify-content-center" class="classynav">
              <ul>
                <li class="nav" id="homeLink"><a href="<?php echo base_url('/') ?>">Beranda</a></li>
                <li class="nav" id="blogLink"><a href="<?php echo base_url('index.php/blog') ?>">Artikel</a></li>
                <!-- ini juga? oh i see -->
                <li id="categoryLink"><a href="#">Kategori</a>
                  <ul class="dropdown">
                    <li><a onclick="semua()" href="<?php echo base_url('index.php/listkuliner') ?>">Semua</a></li>
                    <li><a href="#">Jenis</a>
                      <ul class="dropdown">
                        <li><a href = "<?php echo base_url('index.php/makanan') ?>">Makanan</a></li>
                        <li><a href = "<?php echo base_url('index.php/minuman') ?>" >Minuman</a></li>
                      </ul>
                    </li>

                    <!-- Ini apa ? category ? ya -->
                    <li><a href="#">Fitur</a>
                      <ul class="dropdown">
                        <li><a href = "<?php echo base_url('index.php/halal') ?>"  >Halal</a></li>
                        <li><a href = "<?php echo base_url('index.php/nonhalal') ?>" >Non-Halal</a></li>
                        <li><a href = "<?php echo base_url('index.php/vege') ?>" >Vegetarian</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li class="nav" id="AskLink"><a href="<?php echo base_url('index.php/tanyajawab') ?>">Tanya Ahli Gizi</a></li>
                </li>
                <li class="nav" id="FavLink"><a href="<?php echo base_url('index.php/penjualfav') ?>">Daftar Favorit</a></li>
                </li>
                <li class="nav" id="Promolink"><a href="<?php echo base_url('index.php/promo') ?>">Promo</a></li>
                </li>
                <li class="nav" id="contactLink"><a href="<?php echo base_url('index.php/contact') ?>">Hubungi Kami</a></li>
                </li>
                <li>
                  <div class="search-btn">
                  </div>
                </li>
              </ul>
            </div>
            <!-- Nav End -->
          </div>
        </nav>
      </div>
    </div>
  </div>
</header>

<script type="text/javascript">
// Ganti Judul Jenis Kuliner berdasarkan kategori 
  function semua() {
    href = "<?php echo base_url('index.php/listkuliner') ?>"
    document.getElementById('JenisKuliner').innerText = "Semua Daftar Kuliner"
  }
  function makanan() {
    document.getElementById('JenisKuliner').innerText = "Daftar Kuliner Makanan"
  }
  function minuman() {
    href = "<?php echo base_url('index.php/blog/artikel1') ?>"
    document.getElementById('JenisKuliner').innerText = "Daftar Kuliner Minuman"
  }
  function halal() {
    href = "<?php echo base_url('index.php/listkuliner') ?>"
    document.getElementById('JenisKuliner').innerText = "Daftar Kuliner Halal"
  }
  function nonhalal() {
    href = "<?php echo base_url('index.php/listkuliner') ?>"
    document.getElementById('JenisKuliner').innerText = "Daftar Kuliner Non-Halal"
  }
  function vege() {
    href = "<?php echo base_url('index.php/listkuliner') ?>"
    document.getElementById('JenisKuliner').innerText = "Daftar Kuliner Vegetarian"
  }
  

// Aktifkan Link 
  var pageTitle = "<?php echo $_pageTitle ?>"
  if (pageTitle == "Home") {
    document.getElementById('homeLink').classList.add('active')
  } else if (pageTitle == "About") {
    document.getElementById('aboutLink').classList.add('active')
  } else if (pageTitle == "Blog") {
    document.getElementById('blogLink').classList.add('active')
  } else if (pageTitle == "Contact") {
    document.getElementById('contactLink').classList.add('active')
  } else if (pageTitle == "DaftarKuliner") {
    document.getElementById('categoryLink').classList.add('active')
  } else if (pageTitle == "DetailGizi") {
    document.getElementById('categoryLink').classList.add('active')
  } else if (pageTitle == "DetailPenjual") {
    document.getElementById('categoryLink').classList.add('active')
  } else if (pageTitle == "Artikel") {
    document.getElementById('blogLink').classList.add('active')
  } else if (pageTitle == "Artikel2") {
    document.getElementById('blogLink').classList.add('active')
  } else if (pageTitle == "PenjualFav") {
    document.getElementById('FavLink').classList.add('active')
  } else if (pageTitle == "TanyaJawab") {
    document.getElementById('AskLink').classList.add('active')
  }
  else if (pageTitle == "Promo") {
    document.getElementById('Promolink').classList.add('active')
  }
</script>