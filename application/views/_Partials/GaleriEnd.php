        <!-- ##### Galeri Area Start ##### -->
        <div class="follow-us-instagram">
          <div class="container">
            <div class="row">
              <div class="col-12">
                <h5>Galeri Kuliner</h5>
              </div>
            </div>
          </div>
          <!-- Galeri Feeds -->
          <div class="insta-feeds d-flex flex-wrap">
            <!-- Single Insta Feeds -->
            <div class="single-insta-feeds">
              <img src="<?php echo base_url('assets/img/bg-img/escampur.jpg') ?>" alt="">
              <!-- Icon -->
              <div class="insta-icon">
                <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
              </div>
            </div>
            <!-- Single Insta Feeds -->
            <div class="single-insta-feeds">
              <img src="<?php echo base_url('assets/img/bg-img/martabak.jpg') ?>" alt="">
              <!-- Icon -->
              <div class="insta-icon">
                <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
              </div>
            </div>
            <!-- Single Insta Feeds -->
            <div class="single-insta-feeds">
              <img src="<?php echo base_url('assets/img/bg-img/buburay.jpeg') ?>" alt="">
              <!-- Icon -->
              <div class="insta-icon">
                <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
              </div>
            </div>
            <!-- Single Insta Feeds -->
            <div class="single-insta-feeds">
              <img src="<?php echo base_url('assets/img/bg-img/paris.jpg  ') ?>" alt="">
              <!-- Icon -->
              <div class="insta-icon">
                <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
              </div>
            </div>
            <!-- Single Insta Feeds -->
            <div class="single-insta-feeds">
              <img src="<?php echo base_url('assets/img/bg-img/nasgor.jpg') ?>" alt="">
              <!-- Icon -->
              <div class="insta-icon">
                <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
              </div>
            </div>
            <!-- Single Insta Feeds -->
            <div class="single-insta-feeds">
              <img src="<?php echo base_url('assets/img/bg-img/miebakso.jpg') ?>" alt="">
              <!-- Icon -->
              <div class="insta-icon">
                <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
              </div>
            </div>
            <!-- Single Insta Feeds -->
            <div class="single-insta-feeds">
              <img src="<?php echo base_url('assets/img/bg-img/sate_padang.jpg') ?>" alt="">
              <!-- Icon -->
              <div class="insta-icon">
                <a href="#"><i class="fa fa-search" aria-hidden="true"></i></a>
              </div>
            </div>


          </div>
        </div>
        <!-- ##### Galeri Area End ##### -->