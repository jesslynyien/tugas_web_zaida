<?php

// INI DATA DARI DATABASE, SESUAIKAN NNTI YA.
// LINK KE SECTION FORM
$id_role = isset($row->id_role) ? $row->id_role : false;
$role_name = isset($row->role_name) ? $row->role_name : false;
$role_desc = isset($row->role_desc) ? $row->role_desc	 : false;
$role_cap = isset($row->role_cap) ? $row->role_cap : false;


?>
<div class="container-fluid">
  <div class="row">
    <?php if ($this->session->flashdata('status')) : ?>

      <div class="col-12">
        <div class="alert alert-<?php echo $this->session->flashdata('status') ?> alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <?php echo $this->session->flashdata('message') ?>
        </div>
      </div>

    <?php endif; ?>

    <div class="col-12 col-md-3">
      <h3>Roles</h3>
      <hr>
      <!-- SECTION FORM -->
      <!-- INI INPUT YANG HARUS DI SESUAIKAN DARI DATABASE -->
      <!-- BUAT AJA "INPUT NAME" NYA NGIKUTIN NAMA FIELD DI DATABASE -->
      <!-- $SUBMIT URL ITU  ADA DI CONTROLLER DI METHOD INDEX  -->
      <form method="POST" action="<?php echo base_url($submit_url); ?>">

        <!-- KHUSUS INI NAMENYA JANGAN DI GANTI BIAR KAN PAKAI "id" aja VALUES nya aja diganti -->
        <input type="hidden" name="id" value="<?php echo $id_role ?>">

        <!-- YANG INI DIGANTI SEMUA -->

        <div class="form-group">
          <label for="i-role_name">Name</label>
          <input type="text" class="form-control" name="role_name" id="i-role_name" value="<?php echo $role_name ?>">
        </div>

        <div class="form-group">
          <label for="i-role_desc">Description</label>
          <textarea name="role_desc" id="i-role_desc" rows="    5" class="form-control"><?php echo $role_desc ?></textarea>
        </div>

        <div class="form-group">
          <label for="i-role_cap">Caption</label>
          <input type="text" class="form-control" name="role_cap" id="i-role_cap" value="<?php echo $role_cap ?>">
        </div>
        <!-- SAMPAI SINI -->

        <button type="submit" name="submit" class="btn btn-primary">Save</button>
      </form>
    </div>

    <div class="col-12 col-md-9">

      <table class="table table-bordered">

        <!-- TAMPILKAN NAMA FIELD YANG DIBUTUHKAN DARI DATABSE -->
        <thead>
          <tr>
            <th>ID</th>        
            <th>Name</th>
            <th>Desc</th>
            <th>Caption</th>            
            <th>Action</th>
          </tr>
        </thead>
        <tbody>

          <!-- SESUAI KAN SAMA DATABASE -->
          <?php if ($data->num_rows() > 0) : ?>
            <?php foreach ($data->result() as $d) : ?>
              <tr>
                <td><?php echo $d->id_role ?></td>
                <td><?php echo $d->role_name ?></td>                
                <td><?php echo $d->role_desc ?></td>
                <td><?php echo $d->role_cap ?></td>
                <td>
                  <a class="btn btn-sm btn-danger" href="<?php echo base_url('dashboard/roles/delete/?id=' . $d->id_role) ?>">Delete</a>
                  <a class="btn btn-sm btn-warning" href="<?php echo base_url('dashboard/roles/?id=' . $d->id_role) ?>">Edit</a>
                </td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>