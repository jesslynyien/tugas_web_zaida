<?php

// INI DATA DARI DATABASE, SESUAIKAN NNTI YA.
// LINK KE SECTION FORM
$id_AKG = isset($row->id_AKG) ? $row->id_AKG : false;
$Jenis_Kelamin = isset($row->Jenis_Kelamin) ? $row->Jenis_Kelamin : false;
$GolUsia = isset($row->GolUsia) ? $row->GolUsia : false;
$Energi_kkal = isset($row->Energi_kkal) ? $row->Energi_kkal : false;
$Protein_g = isset($row->Protein_g) ? $row->Protein_g : false;
$Lemak_g = isset($row->Lemak_g) ? $row->Lemak_g : false;
$Karbo_g = isset($row->Karbo_g) ? $row->Karbo_g : false;

?>
<div class="container-fluid">
  <div class="row">
    <?php if ($this->session->flashdata('status')) : ?>

      <div class="col-12">
        <div class="alert alert-<?php echo $this->session->flashdata('status') ?> alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <?php echo $this->session->flashdata('message') ?>
        </div>
      </div>

    <?php endif; ?>

    <div class="col-12 col-md-3">
      <h3>AKG</h3>
      <hr>
      <!-- SECTION FORM -->
      <!-- INI INPUT YANG HARUS DI SESUAIKAN DARI DATABASE -->
      <!-- BUAT AJA "INPUT NAME" NYA NGIKUTIN NAMA FIELD DI DATABASE -->
      <!-- $SUBMIT URL ITU  ADA DI CONTROLLER DI METHOD INDEX  -->
      <form method="POST" action="<?php echo base_url($submit_url); ?>">

        <!-- KHUSUS INI NAMENYA JANGAN DI GANTI BIAR KAN PAKAI "id" aja VALUES nya aja diganti -->
        <input type="hidden" name="id" value="<?php echo $id_AKG ?>">

        <!-- YANG INI DIGANTI SEMUA -->

        <div class="form-group">
          <label for="i-Jenis_Kelamin">Jenis Kelamin</label>
          <input type="text" class="form-control" name="Jenis_Kelamin" id="i-Jenis_Kelamin" value="<?php echo $Jenis_Kelamin ?>">
        </div>

        <div class="form-group">
          <label for="i-GolUsia">GolUsia</label>
          <input type="text" class="form-control" name="GolUsia" id="i-GolUsia" value="<?php echo $GolUsia ?>">
        </div>

        <div class="form-group">
          <label for="i-Energi_kkal">Energi ( kkal )</label>
          <input type="text" class="form-control" name="Energi_kkal" id="i-Energi_kkal" value="<?php echo $Energi_kkal ?>">
        </div>
        
        <div class="form-group">
          <label for="i-Protein_g">Protein (g)</label>
          <input type="text" class="form-control" name="Protein_g" id="i-Protein_g" value="<?php echo $Protein_g ?>">
        </div>

        <div class="form-group">
          <label for="i-Lemak_g">Lemak</label>
          <input type="text" class="form-control" name="Lemak_g" id="i-Lemak_g" value="<?php echo $Lemak_g ?>">
        </div>

        <div class="form-group">
          <label for="i-Karbo_g">Karbohidrat (g)</label>
          <input type="text" class="form-control" name="Karbo_g" id="i-Karbo_g" value="<?php echo $Karbo_g ?>">
        </div>
        <!-- SAMPAI SINI -->

        <button type="submit" name="submit" class="btn btn-primary">Save</button>
      </form>
    </div>

    <div class="col-12 col-md-9">

      <table class="table table-bordered">

        <!-- TAMPILKAN NAMA FIELD YANG DIBUTUHKAN DARI DATABSE -->
        <thead>
          <tr>
            <th>ID</th>
            <th>Jenis Kelamin</th>            
            <th>Gol Usia</th>            
            <th>Energi ( kkal )</th>
            <th>Protein</th>
            <th>Lemak</th>
            <th>Karbohidrat</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>

          <!-- SESUAI KAN SAMA DATABASE -->
          <?php if ($data->num_rows() > 0) : ?>
            <?php foreach ($data->result() as $d) : ?>
              <tr>
                <td><?php echo $d->id_AKG ?></td>
                <td><?php echo $d->Jenis_Kelamin ?></td>                
                <td><?php echo $d->GolUsia ?></td>                
                <td><?php echo $d->Energi_kkal ?></td>
                <td><?php echo $d->Protein_g ?></td>
                <td><?php echo $d->Lemak_g ?></td>
                <td><?php echo $d->Karbo_g ?></td>
                
                <td>
                  <a class="btn btn-sm btn-danger" href="<?php echo base_url('dashboard/akg/delete/?id=' . $d->id_AKG) ?>">Delete</a>
                  <a class="btn btn-sm btn-warning" href="<?php echo base_url('dashboard/akg/?id=' . $d->id_AKG) ?>">Edit</a>
                </td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>