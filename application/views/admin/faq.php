<?php

// INI DATA DARI DATABASE, SESUAIKAN NNTI YA.
// LINK KE SECTION FORM
$id_faq = isset($row->id_faq) ? $row->id_faq : false;
$faq_title = isset($row->faq_title) ? $row->faq_title : false;
$faq_content = isset($row->faq_content) ? $row->faq_content : false;


?>
<div class="container-fluid">
  <div class="row">
    <?php if ($this->session->flashdata('status')) : ?>

      <div class="col-12">
        <div class="alert alert-<?php echo $this->session->flashdata('status') ?> alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <?php echo $this->session->flashdata('message') ?>
        </div>
      </div>

    <?php endif; ?>

    <div class="col-12 col-md-3">
      <h3>FAQ</h3>
      <hr>
      <!-- SECTION FORM -->
      <!-- INI INPUT YANG HARUS DI SESUAIKAN DARI DATABASE -->
      <!-- BUAT AJA "INPUT NAME" NYA NGIKUTIN NAMA FIELD DI DATABASE -->
      <!-- $SUBMIT URL ITU  ADA DI CONTROLLER DI METHOD INDEX  -->
      <form method="POST" action="<?php echo base_url($submit_url); ?>">

        <!-- KHUSUS INI NAMENYA JANGAN DI GANTI BIAR KAN PAKAI "id" aja VALUES nya aja diganti -->
        <input type="hidden" name="id" value="<?php echo $id_cat ?>">

        <!-- YANG INI DIGANTI SEMUA -->

        <div class="form-group">
          <label for="i-faq_title">Title</label>
          <input type="text" class="form-control" name="faq_title" id="i-faq_title" value="<?php echo $faq_title ?>">
        </div>

        <div class="form-group">
          <label for="i-faq_content">Content</label>
          <textarea name="cat_desc" id="i-faq_content" rows="5" class="form-control"><?php echo $faq_content ?></textarea>
        </div>
        <!-- SAMPAI SINI -->

        <button type="submit" name="submit" class="btn btn-primary">Save</button>
      </form>
    </div>

    <div class="col-12 col-md-9">

      <table class="table table-bordered">

        <!-- TAMPILKAN NAMA FIELD YANG DIBUTUHKAN DARI DATABSE -->
        <thead>
          <tr>
            <th>ID</th>
            <th>Title</th>            
            <th>Content</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>

          <!-- SESUAI KAN SAMA DATABASE -->
          <?php if ($data->num_rows() > 0) : ?>
            <?php foreach ($data->result() as $d) : ?>
              <tr>
                <td><?php echo $d->id_faq ?></td>
                <td><?php echo $d->faq_title ?></td>                
                <td><?php echo $d->faq_content ?></td>
                <td>
                  <a class="btn btn-sm btn-danger" href="<?php echo base_url('dashboard/faq/delete/?id=' . $d->id_faq) ?>">Delete</a>
                  <a class="btn btn-sm btn-warning" href="<?php echo base_url('dashboard/faq/?id=' . $d->id_faq) ?>">Edit</a>
                </td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>