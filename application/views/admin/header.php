<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="<?php echo base_url('plugins/tinymce/js/tinymce/tinymce.min.js') ?>"></script>
    <script>
         tinymce.init({
            selector: '#editor'
        });
    </script>
  </head>
  <body>

    <nav class="navbar navbar-expand-sm navbar-light bg-light mb-3">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavId">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('dashboard/user')?>">User</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('dashboard/post?post_type=page')?>">Page</a>
                </li>
              
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Post</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownId">
                        <a class="dropdown-item" href="<?php echo base_url('dashboard/post?post_type=post&taxonomy=category&cat_type=multiple') ?>">Post Data</a>
                        <a class="dropdown-item" href="<?php echo base_url('dashboard/category?taxonomy=category'); ?>">Category</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Kuliner</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownId">
                        <a class="dropdown-item" href="<?php echo base_url('dashboard/post?post_type=kuliner&taxonomy=kuliner&cat_type=multiple') ?>">Kuliner Data</a>
                        <a class="dropdown-item" href="<?php echo base_url('dashboard/category?taxonomy=kuliner'); ?>">Kuliner Category</a>
                    </div>

                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Sliders</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownId">
                        <a class="dropdown-item" href="<?php echo base_url('dashboard/post?post_type=slider&taxonomy=slider&cat_type=single') ?>">Slider</a>
                        <a class="dropdown-item" href="<?php echo base_url('dashboard/category?taxonomy=slider'); ?>">Slider Category</a>
                    </div>
                </li>

                <!-- <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('dashboard/menu')?>">Menu</a>
                </li> -->
            </ul>
            <!-- <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form> -->
        </div>
    </nav>