<?php

// INI DATA DARI DATABASE, SESUAIKAN NNTI YA.
// LINK KE SECTION FORM
$id_user = isset($row->id_user) ? $row->id_user : false;
$username = isset($row->username) ? $row->username : false;
$email_user = isset($row->email_user) ? $row->email_user : false;
$password_user = isset($row->password_user) ? $row->password_user : false;
$fullname_user = isset($row->fullname_user) ? $row->fullname_user : false;
$id_role = isset($row->id_role) ? $row->id_role : false;
$pengguna_status = isset($row->pengguna_status) ? $row->pengguna_status : false;

?>
<div class="container-fluid">
  <div class="row">
    <?php if ($this->session->flashdata('status')) : ?>

      <div class="col-12">
        <div class="alert alert-<?php echo $this->session->flashdata('status') ?> alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <?php echo $this->session->flashdata('message') ?>
        </div>
      </div>

    <?php endif; ?>

    <div class="col-12 col-md-3">
      <h3>User</h3>
      <hr>
      <!-- SECTION FORM -->
      <!-- INI INPUT YANG HARUS DI SESUAIKAN DARI DATABASE -->
      <!-- BUAT AJA "INPUT NAME" NYA NGIKUTIN NAMA FIELD DI DATABASE -->
      <!-- $SUBMIT URL ITU  ADA DI CONTROLLER DI METHOD INDEX  -->
      <form method="POST" action="<?php echo base_url($submit_url); ?>">

        <!-- KHUSUS INI NAMENYA JANGAN DI GANTI IAR KAN PAKAI "id" aja VALUES nya aja diganti -->
        <input type="hidden" name="id" value="<?php echo $id_user ?>">

        <!-- YANG INI DIGANTI SEMUA -->
        <div class="form-group">
          <label for="i-username">Username</label>
          <input type="text" class="form-control" name="username" id="i-username" value="<?php echo $username ?>">
        </div>

        <div class="form-group">
          <label for="i-email_user">Email</label>
          <input type="text" class="form-control" name="email_user" id="i-email_user" value="<?php echo $email_user ?>">
        </div>

        <div class="form-group">
          <label for="i-password_user">Password</label>
          <input type="password" class="form-control" name="password_user" id="i-password_user" value="<?php echo ""; ?>">
        </div>
        
        <div class="form-group">
          <label for="i-fullname_user">Fullname</label>
          <input type="text" class="form-control" name="fullname_user" id="i-fullname_user" value="<?php echo $fullname_user ?>">
        </div>
        <div class="form-group">
          <label for="i-id_role">Roles</label>
          <select name="role" id="i-id_role" class="form-control">
          <?php $roles = $this->db->select()->from('roles')->get(); ?>
          <?php if($roles->num_rows() > 0 ) : ?>

              <?php foreach($roles->result() as $role) : ?>
              
              <option value="<?php echo $role->id_role ?>" <?php if($id_role == $role->id_role) echo "selected"; ?>><?php echo $role->role_name; ?></option>
              
              <?php endforeach ?>


          <?php endif; ?>
          </select>
          <!-- <input type="text" class="form-control" name="id_role" id="i-id_role" value="<?php echo $id_role ?>"> -->
        </div>
        <div class="form-group">
          <label for="i-pengguna_status">Status</label>
          <select name="status_pengguna" id="i-pengguna_status" class="form-control">
              <option value="">Choose</option>
              <option value="1" <?php if($pengguna_status == 1) echo "selected"; ?>>Active</option>
              <option value="0" <?php if($pengguna_status == 0) echo "selected"; ?>>Pending</option>
          </select>
       
        </div>

        <!-- SAMPAI SINI -->

        <button type="submit" name="submit" class="btn btn-primary">Save</button>
      </form>
    </div>

    <div class="col-12 col-md-9">

      <table class="table table-bordered">

        <!-- TAMPILKAN NAMA FIELD YANG DIBUTUHKAN DARI DATABSE -->
        <thead>
          <tr>
            <th>ID</th>
            <th>Username</th>
            <th>Email</th>
            <th>Fullname</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>

          <!-- SESUAI KAN SAMA DATABASE -->
          <?php if ($data->num_rows() > 0) : ?>
            <?php foreach ($data->result() as $d) : ?>
              <tr>
                <td><?php echo $d->id_user ?></td>
                <td><?php echo $d->username ?></td>
                <td><?php echo $d->email_user ?></td>
                <td><?php echo $d->fullname_user ?></td>
                <td>
                  <?php if($d->pengguna_status == 1) echo "Active";  ?>
                  <?php if($d->pengguna_status == 0) echo "Pending";  ?>
                </td>
                <td>
                  <a class="btn btn-sm btn-danger" href="<?php echo base_url('dashboard/user/delete/?id=' . $d->id_user) ?>">Delete</a>
                  <a class="btn btn-sm btn-warning" href="<?php echo base_url('dashboard/user/?id=' . $d->id_user) ?>">Edit</a>
                </td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>

        </tbody>
      </table>


    </div>
  </div>
</div>