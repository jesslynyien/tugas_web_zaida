<?php

// INI DATA DARI DATABASE, SESUAIKAN NNTI YA.
// LINK KE SECTION FORM
$id_inbox = isset($row->id_inbox) ? $row->id_inbox : false;
$fullname = isset($row->fullname) ? $row->fullname : false;
$email = isset($row->email) ? $row->email : false;
$subject = isset($row->subject) ? $row->subject : false;
$message = isset($row->message) ? $row->message : false;

?>
<div class="container-fluid">
  <div class="row">
    <?php if ($this->session->flashdata('status')) : ?>

      <div class="col-12">
        <div class="alert alert-<?php echo $this->session->flashdata('status') ?> alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            <span class="sr-only">Close</span>
          </button>
          <?php echo $this->session->flashdata('message') ?>
        </div>
      </div>

    <?php endif; ?>

    <div class="col-12 col-md-3">
      <h3>Inbox</h3>
      <hr>
      <!-- SECTION FORM -->
      <!-- INI INPUT YANG HARUS DI SESUAIKAN DARI DATABASE -->
      <!-- BUAT AJA "INPUT NAME" NYA NGIKUTIN NAMA FIELD DI DATABASE -->
      <!-- $SUBMIT URL ITU  ADA DI CONTROLLER DI METHOD INDEX  -->
      <form method="POST" action="<?php echo base_url($submit_url); ?>">

        <!-- KHUSUS INI NAMENYA JANGAN DI GANTI BIAR KAN PAKAI "id" aja VALUES nya aja diganti -->
        <input type="hidden" name="id" value="<?php echo $id_inbox ?>">

        <!-- YANG INI DIGANTI SEMUA -->

        <div class="form-group">
          <label for="i-fullname">Fullname</label>
          <input type="text" class="form-control" name="fullname" id="i-fullname" value="<?php echo $fullname ?>">
        </div>

        <div class="form-group">
          <label for="i-email">Email</label>
          <input type="text" class="form-control" name="email" id="i-email" value="<?php echo $email ?>">
        </div>
        
        <div class="form-group">
          <label for="i-subject">Subject</label>
          <input type="text" class="form-control" name="subject" id="i-subject" value="<?php echo $subject ?>">
        </div>

        <div class="form-group">
          <label for="i-message">Message</label>
          <textarea name="message" id="i-message" rows="5" class="form-control"><?php echo $message ?></textarea>
        </div>
        <!-- SAMPAI SINI -->

        <button type="submit" name="submit" class="btn btn-primary">Save</button>
      </form>
    </div>

    <div class="col-12 col-md-9">

      <table class="table table-bordered">

        <!-- TAMPILKAN NAMA FIELD YANG DIBUTUHKAN DARI DATABSE -->
        <thead>
          <tr>
            <th>ID</th>
            <th>Fullname</th>
            <th>Email</th>
            <th>Subject</th>
            <th>Message</th>            
            <th>Action</th>
          </tr>
        </thead>
        <tbody>

          <!-- SESUAI KAN SAMA DATABASE -->
          <?php if ($data->num_rows() > 0) : ?>
            <?php foreach ($data->result() as $d) : ?>
              <tr>
                <td><?php echo $d->id_inbox ?></td>
                <td><?php echo $d->fullname ?></td>                
                <td><?php echo $d->email ?></td>
                <td><?php echo $d->subject ?></td>
                <td><?php echo $d->message ?></td>                
                <td>
                  <a class="btn btn-sm btn-danger" href="<?php echo base_url('dashboard/inbox/delete/?id=' . $d->id_inbox) ?>">Delete</a>
                  <a class="btn btn-sm btn-warning" href="<?php echo base_url('dashboard/inbox/?id=' . $d->id_inbox) ?>">Edit</a>
                </td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>