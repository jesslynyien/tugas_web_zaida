<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("_Partials/Head.php") ?>
</head>

<body>
  <!-- Preloader -->
  <?php $this->load->view("_Partials/Preloader.php") ?>

  <!-- Search Wrapper -->
  <?php $this->load->view("_Partials/SearchWrapper.php") ?>

  <!-- ##### Header Area Start ##### -->
  <?php $this->load->view("_Partials/HeaderArea.php") ?>
  <!-- ##### Header Area End ##### -->

  <!-- ##### Breadcumb Area Start ##### -->
  <div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?= base_url() ?>assets/img/bg-img/sate_memeng.jpg);">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcumb-text text-center">
            <h2>Artikel</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ##### Breadcumb Area End ##### -->

  <!-- ##### Blog Area Start ##### -->
  <div class="blog-area section-padding-80">
    <div class="container">
      <div class="row">
        <div class="col-8 col-md-8">
          <!-- Judul  -->
          <div class="receipe-headline">
            <span>12 Desember 2019</span>
            <h3>Yang Penting Gizi Seimbang, Bukan "4 Sehat 5 Sempurna" </h3>
            <div class="receipe-duration">
              <blockquote><i><b>Dok, Mengapa ya, berat badan anak saya tidak bisa bertambah walaupun sudah banyak makan dan makanan yang saya sediakan itu makanan 4 sehat 5 sempurna ?</b></i></blockquote>
              <i>~ Rose, 29 tahun</i>
            </div>
          </div>

          <p  style="color:black;">
            Kita telah mengenal jargon 4 sehat 5 sempurna jauh sebelum kita benar-benar belajar formal tentang makanan sehat di bangku sekolah. Slogan 4 sehat 5 sempurna ini dipopulerkan oleh guru besar ilmu gizi pertama di Indonesia, Prof. Poerwo Soedarmo pada tahun 1950-an.</p>
            
          <!-- Gambar  -->
          <div class="container">
            <div class="row">
              <div class="col-12">
                <div class="receipe-slider owl-carousel">
                  <img src="<?php echo base_url('assets/img/bg-img/4sehat.jpg') ?>" alt="">
                  <img src="<?php echo base_url('assets/img/bg-img/4sehat.jpg') ?>" alt="">
                </div>
              </div>
            </div>
          </div>

          <p style="color:black;" class="mt-4">Dalam konsep 4 sehat 5 sempurna, makanan sehat adalah makanan yang mengandung 4 sumber nutrisi yaitu makanan pokok, lauk pauk, sayur-sayuran, buah-buahan, dan disempurnakan dengan susu.</p>

          <p  style="color:black;">Sejak tahun 1990-an pedoman 4 sehat 5 sempurna ini dianggap tak lagi sesuai dengan perkembangan ilmu pengetahuan dan teknologi gizi. Hingga kemudian, pemerintah melalui Kementerian Kesehatan akhir bulan Oktober lalu mengkampanyekan slogan "Isi Piringku" sebagai pengganti slogan "4 Sehat 5 Sempurna" untuk pedoman konsumsi sehari-hari dalam memenuhi gizi seimbang.</p>

          <p  style="color:black;">"Dulu kita punya slogan 4 Sehat 5 Sempurna, namun dalam perkembangan ilmu gizi tidak cukup tepat untuk mengakomodir perkembangan ilmu yang baru. Kalau hanya bicara 4 Sehat 5 Sempurna tanpa keseimbangan itu tidak cukup," kata Direktur Jenderal Kesehatan Masyarakat Kementerian Kesehatan Anung Sugihantono dalam konferensi pers acara Forum Pangan Asia Pasifik.</p>

          <p  style="color:black;">Secara umum, "Isi Piringku" menggambarkan porsi makan yang dikonsumsi dalam satu piring yang terdiri dari 50 persen buah dan sayur, dan 50 persen sisanya terdiri dari karbohidrat dan protein.</p>

          <p  style="color:black;">Kampanye "Isi Piringku" juga menekankan untuk membatasi gula, garam, dan lemak dalam konsumsi sehari-hari. Dalam perkembangan ilmu gizi yang baru, pedoman "4 Sehat 5 Sempurna" berubah menjadi pedoman gizi seimbang yang terdiri dari 10 pesan tentang menjaga gizi.</p>

          <p  style="color:black;">Dari 10 pesan tersebut, Anung mengelompokkan lagi menjadi empat pesan pokok yakni pola makan gizi seimbang, minum air putih yang cukup, aktivitas fisik minimal 30 menit per hari, dan mengukur tinggi dan berat badan yang sesuai untuk mengetahui kondisi tubuh.</p>

          <p  style="color:black;">Selain diagram "Isi Piringku" yang telah disebutkan, kampanye tersebut juga menekankan empat hal penting lainnya yaitu cuci tangan sebelum makan, aktivitas fisik yang cukup, minum air putih cukup, dan memantau tinggi badan dan berat badan.
          </p>


          <p  style="color:black;">Sementara itu, kasus obesitas pada anak Indonesia mulai menjadi perhatian banyak pihak. Hal ini dikarenakan tingginya prevalensi obesitas di Indonesia.</p>

          <p  style="color:black;">C.N. Rachmi, peneliti dari Fakultas Kedokteran Universitas Padjadjaran dan pemerhati kesehatan anak di Children's Hospital at Westmead, University of Sydney Clinical School, Sydney, dalam penelitiannya yang berjudul "Overweight and Obesity in Indonesia: Prevalence and Risk Factors" yang dipublikasikan 2017, menyatakan bahwa dari seluruh negara di Asia Tenggara, prevalensi obesitas di Indonesia adalah yang paling tinggi.</p>

          <p style="color:black;">Data Riset Kesehatan Nasional 2016 diketahui bahwa 20,7 persen penduduk dewasa Indonesia mengalami kegemukan. Data World Health Organization (WHO) pada 2013 menunjukkan hampir 12 persen anak Indonesia mengalami obesitas. Jika dirinci lagi, dari 17 juta anak yang mengalami obesitas di ASEAN, hampir 7 jutanya berasal dari Indonesia. Angka ini hanya mencakup balita.</p>

          <p style="color:black;">Obesitas pada anak dapat terjadi karena faktor keturunan, kurangnya aktivitas fisik, dan pola makan. Pola makan tentu terkait dengan kadar nutrisi yang dikonsumsi setiap harinya yang dapat dilihat dari Angka Kecukupan Gizi (AKG). AKG yang dianjurkan untuk masyarakat Indonesia telah diatur dalam Peraturan Menkes RI Nomor 75 tahun 2013.</p>

          <p style="color:black;">Anjuran kisaran sebaran energi gizi makro (AMDR) bagi penduduk Indonesia dalam estimasi kecukupan gizi ini adalah 5-15 persen energi protein, 25-35 persen energi lemak, dan 40-60 persen energi karbohidrat. Penerapannya tergantung umur atau tahap pertumbuhan dan perkembangan.</p>

          <p style="color:black;">Sebagai penggambaran pemenuhan gizi sekali makan, Rio Jati mencontohkan susunan menu per piring berupa 2 centong rice cooker nasi sejumlah kira-kira 100 gram, sayur bening bayam 100 gram, jeruk 1 buah, dan ikan goreng bagian badan 1 potong.</p>

          <h5> Oleh : dr.Yulaika Ramadhan</h5>

          
          <a href="<?php echo base_url('index.php/penjualfav') ?>" class="btn delicious-btn mt-2"><i class="fa fa-plus" aria-hidden="true"></i> Tambahkan ke Favorit</a>

        </div>
        <div class="col-4 col-lg-4">
          <div class="blog-sidebar-area">

            <!-- Widget -->
            <div class="single-widget mb-80">
              <h6>Arsip</h6>
              <ul class="list">
                <li><a href="#">Maret 2018</a></li>
                <li><a href="#">Februari 2018</a></li>
                <li><a href="#">Januari 2018</a></li>
                <li><a href="#">Desember 2017</a></li>
                <li><a href="#">November 2017</a></li>
              </ul>
            </div>

            <!-- Widget -->
            <div class="single-widget mb-80">
              <h6>Kategori</h6>
              <ul class="list">
                <li><a href="#">Kehamilan</a></li>
                <li><a href="#">Gizi Anak</a></li>
                <li><a href="#">Kekurangan Gizi</a></li>
                <li><a href="#">Belum Terkategori</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>
  <!-- ##### Blog Area End ##### -->

  <section class="quote-subscribe-addsn mt-5">
    <div class="container">
      <div class="row align-items-end">
        <!-- ##### Follow Us Instagram Area Start ##### -->
        <?php $this->load->view("_Partials/GaleriEnd.php") ?>
        <!-- ##### Follow Us Instagram Area End ##### -->

        <!-- ##### Footer Area Start ##### -->
        <?php $this->load->view("_Partials/Footer.php") ?>
        <!-- ##### Footer Area Start ##### -->

      </div>
    </div>
  </section>
  <!-- ##### All Javascript Files ##### -->
  <?php $this->load->view("_Partials/Js.php") ?>
</body>

</html>