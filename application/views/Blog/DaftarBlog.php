<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("_Partials/Head.php") ?>
</head>

<body>
  <!-- Preloader -->
  <?php $this->load->view("_Partials/Preloader.php") ?>

  <!-- Search Wrapper -->
  <?php $this->load->view("_Partials/SearchWrapper.php") ?>

  <!-- ##### Header Area Start ##### -->
  <?php $this->load->view("_Partials/HeaderArea.php") ?>
  <!-- ##### Header Area End ##### -->

  <!-- ##### Breadcumb Area Start ##### -->
  <div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?= base_url() ?>assets/img/bg-img/jalankaki.jpg);">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcumb-text text-center">
            <h2>Daftar Artikel</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ##### Breadcumb Area End ##### -->

  <!-- ##### Blog Area Start ##### -->
  <div class="blog-area section-padding-80">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-8">
          <div class="blog-posts-area">
            <!-- Single Blog Area -->
            <div class="single-blog-area mb-80">
              <!-- Thumbnail -->
              <div class="blog-thumbnail">
                <img src="<?php echo base_url('assets/img/bg-img/4sehat.jpg') ?>" alt="">
                <!-- Post Date -->
                <div class="post-date">
                  <a href="#"><span>12</span>Des<br> 2019</a>
                </div>
              </div>
              <!-- Content -->
              <div class="blog-content">
                <a href="<?php echo base_url('index.php/blog/artikel2') ?>" class="post-title">Yang Penting Gizi Seimbang, Bukan "4 Sehat 5 Sempurna"</a>
                <div class="meta-data">oleh  <a href="#">dr.Yulaika Ramadhan</a>
                </div>
                <p>Jus sehat menjadi salah satu cara yang menyenangkan dalam menikmati buah dan sayuran. Meski kehilangan sebagian serat, tapi jus dari buah dan sayur ini masih tetap mengandung sebagian besar vitamin, mineral, fitonutrien. Lantas, manfaat apa saja yang bisa di peroleh dari mengonsumsi jus sehat?</p>
                <!-- <a href="<?php echo base_url('index.php/blog/artikel') ?>" class="btn delicious-btn mt-30">Baca Lebih Lanjut</a> -->
              </div>
            </div>
            <!-- Single Blog Area -->
            <div class="single-blog-area mb-80">
              <!-- Thumbnail -->
              <div class="blog-thumbnail">
                <img src="<?php echo base_url('assets/img/bg-img/jalankaki.jpg') ?>" alt="">
                <!-- Post Date -->
                <div class="post-date">
                  <a href="#"><span>11</span>Des<br> 2019</a>
                </div>
              </div>
              <!-- Content -->
              <div class="blog-content">
                <a href="<?php echo base_url('index.php/blog/artikel') ?>" class="post-title">Rutin Jalan Kaki Saat Hamil, Bunda Akan Merasakan 5 Manfaat ini</a>
                <div class="meta-data">oleh <a href="#">dr. Giasinta</a>
                </div>
                <p>Jalan kaki saat hamil mungkin tak terpikirkan untuk dilakukan oleh Bunda karena sudah merasa kelelahan membawa bayi dalam kandungan. Padahal, jalan kaki amat direkomendasikan untuk dilakukan selama kehamilan dan baik bagi janin dalam kandungan...</p>
              </div>
            </div>



            <!-- Single Blog Area -->
            <div class="single-blog-area mb-80">
              <!-- Thumbnail -->
              <div class="blog-thumbnail">
                <img src="<?php echo base_url('assets/img/blog-img/3.jpg') ?>" alt="">
                <!-- Post Date -->
                <div class="post-date">
                  <a href="#"><span>05</span>April <br> 2018</a>
                </div>
              </div>
              <!-- Content -->
              <div class="blog-content">
                <a href="<?php echo base_url('index.php/blog/artikel2') ?>" class="post-title">Begini Cara Membuat Burger yang Lezat Tapi Sehat dan Padat Gizi </a>
                <div class="meta-data">by <a href="#">Maria Williams</a> in <a href="#">Restaurants</a>
                </div>
                <p>Siapa bilang Anda tidak bisa membuat burger yang enak seperti gerai fast food di luar sana? Anda juga bisa, kok! Burger bukan hanya makanan cepat saji yang bisa Anda beli saat buru-buru, tapi Anda juga bisa membuatnya sendiri dengan cara yang mudah. Bahkan menurut Penny Kris-Etherton, Ph.D., RD, seorang ahli gizi sekaligus peneliti dari Penn State University, burger bisa jadi makanan yang sehat. Lantas, bagaimana cara membuat burger yang sehat nan lezat? Baca terus ulasan berikut! </p>
              </div>
            </div>
            <!-- Page Navigation -->
            <div class="justify-content-center mb-5">
              <nav aria-label="Page navigation">
                <ul class="pagination justify-content-center">
                  <li class="page-item disabled">
                    <a class="page-link" href="#" aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                      <span class="sr-only">Previous</span>
                    </a>
                  </li>
                  <nav aria-label="Page navigation example">
                    <ul class="pagination">
                      <li class="page-item active"><a class="page-link" href="#">01.</a></li>
                      <li class="page-item"><a class="page-link" href="#">02.</a></li>
                      <li class="page-item"><a class="page-link" href="#">03.</a></li>
                    </ul>
                  </nav>
                  <li class="page-item active"><a class="page-link" href="#"></a></li>
                  <li class="page-item"><a class="page-link" href="#"></a></li>
                  <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                      <span class="sr-only">Next</span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>

        <div class="col-12 col-lg-4">
          <div class="blog-sidebar-area">

            <!-- Widget -->
            <div class="single-widget mb-80">
              <h6>Arsip</h6>
              <ul class="list">
                <li><a href="#">April 2019</a></li>
                <li><a href="#">Mei 2019</a></li>
                <li><a href="#">Juli 2019</a></li>
                <li><a href="#">Agustus 2019</a></li>
                <li><a href="#">September 2019</a></li>
                <li><a href="#">Oktober 2019</a></li>
                <li><a href="#">November 2019</a></li>
                <li><a href="#">Desember 2019</a></li>
              </ul>
            </div>

            <!-- Widget -->
            <div class="single-widget mb-80">
              <h6>Kategori</h6>
              <ul class="list">
                <li><a href="#">Kehamilan</a></li>
                <li><a href="#">Gizi Anak</a></li>
                <li><a href="#">Kekurangan Gizi</a></li>
                <li><a href="#">Belum Terkategori</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ##### Blog Area End ##### -->


  <section class="quote-subscribe-addsn mt-5">
    <div class="container">
      <div class="row align-items-end">
        <!-- ##### Follow Us Instagram Area Start ##### -->
        <?php $this->load->view("_Partials/GaleriEnd.php") ?>
        <!-- ##### Follow Us Instagram Area End ##### -->

        <!-- ##### Footer Area Start ##### -->
        <?php $this->load->view("_Partials/Footer.php") ?>
        <!-- ##### Footer Area Start ##### -->
      </div>
    </div>
  </section>

  <!-- ##### All Javascript Files ##### -->
  <?php $this->load->view("_Partials/Js.php") ?>
</body>

</html>