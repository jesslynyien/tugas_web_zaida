<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("_Partials/Head.php") ?>
</head>

<body>
  <!-- Preloader -->
  <?php $this->load->view("_Partials/Preloader.php") ?>

  <!-- Search Wrapper -->
  <?php $this->load->view("_Partials/SearchWrapper.php") ?>

  <!-- ##### Header Area Start ##### -->
  <?php $this->load->view("_Partials/HeaderArea.php") ?>
  <!-- ##### Header Area End ##### -->

  <!-- ##### Breadcumb Area Start ##### -->
  <div class="breadcumb-area bg-img bg-overlay" style="background-image: url(<?= base_url() ?>assets/img/bg-img/nasgor.jpg);">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcumb-text text-center">
            <h2>Tanya Ahli Gizi</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ##### Breadcumb Area End ##### -->

  <!-- ##### Tabs ##### -->
  <section class="elements-area section-padding-80-0">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-12">
          <div class="delicious-tabs-content">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <!-- Tab 1  -->
              <li class="nav-item">
                <a class="nav-link active" id="tab--1" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="false">Membuat Pertanyaan</a>
              </li>
              <!-- Tab 2 -->
              <li class="nav-item">
                <a class="nav-link" id="tab--2" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="false">Daftar Pertanyaan</a>
              </li>
            </ul>

            <div class="tab-content mb-80" id="myTabContent">
              <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab--1">
                <div class="delicious-tab-content">
                  <!-- Tab Text 1-->
                  <!-- ##### Pertanyaan Start ##### -->
                  <div class="contact-area section-padding-0-70 mt-5">
                    <div class="container">
                      <div class="row">
                        <div class="col-12">
                          <div class="section-heading font-weight-bold">
                            <h5> Pertanyaan Terpilih akan dijawab dalam Artikel oleh Ahli Gizi terpercaya. Anda akan mendapat pemberitahuan jika pertanyaan Anda terpilih</h5>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-12">
                          <div class="contact-form-area">
                            <form action="#" method="post">
                              <div class="row">
                                <div class="col-12 col-lg-6">
                                  <input type="text" class="form-control font-weight-bold" id="name" placeholder="Nama">
                                </div>
                                <div class="col-12 col-lg-6">
                                  <input type="number" class="form-control font-weight-bold" id="usia" placeholder="Usia">
                                </div>
                                <div class="col-12">
                                  <input type="text" class="form-control font-weight-bold" id="subject" placeholder="Judul/Topik">
                                </div>
                                <div class="col-12">
                                  <textarea name="message" class="form-control font-weight-bold" id="message" cols="30" rows="8" placeholder="Pertanyaan"></textarea>
                                </div>
                                <div class="col-12 text-center">
                                  <button class="btn delicious-btn mt-30" type="submit">Tanyakan</button>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- ##### Pertanyaan End ##### -->
                </div>
              </div>
              <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab--2">
                <div class="delicious-tab-content">
                  <!-- Tab Text 2 -->
                  <div class="delicious-tab-text">
                    <div class="single-preparation-step d-flex mb-2">
                      <h4>01.</h4>
                      <h6 class="ml-4"> Dok, Apa iya rutinitas jalan kaki saat hamil juga perlu disamping makan makanan bergizi bagi bayi ? Berapa lama durasi jalan kaki yang dianjurkan per hari?
                        <hr>
                        ( 5 Desember, 2019 )
                      </h6>
                    </div>
                    <!-- Alert -->
                    <div class="alert alert-warning clearfix" role="alert">
                      <strong><i class="fa fa-check mr-3" aria-hidden="true"></i><a href="<?php echo base_url('index.php/artikel') ?>">Masih diproses</a> </strong>
                    </div>

                    <div class="single-preparation-step d-flex mb-2">
                      <h4>02.</h4>
                      <h6 class="ml-4"> Dok, Apa benar kalau mengidam adalah sinyal bahwa tubuh kekurangan nutrisi tertentu bagi ibu hamil?
                        <hr>
                        ( 5 Desember, 2019 )
                      </h6>
                    </div>
                    <!-- Alert -->
                    <div class="alert alert-warning clearfix" role="alert">
                      <strong><i class="fa fa-check mr-3" aria-hidden="true"></i><a href="<?php echo base_url('index.php/artikel') ?>">Masih diproses</a> </strong>
                    </div>

                    <div class="single-preparation-step d-flex mb-2">
                      <h4>03.</h4>
                      <h6 class="ml-4">  Dok, anak saya lahir prematur dengan berat badan 1700 gram. Sekarang usianya 22 bulan dengan berat badan 12 kg. Seminggu yang lalu ia terkena cacar air dan nafsu makannya menurun. Pola makan yang baik yang bagaimana untuk pertumbuhan anak prematur? Apa sama dengan anak normal? Terima kasih.
                        <hr>
                        ( 5 Desember, 2019 )
                      </h6>
                    </div>
                    <!-- Alert -->
                    <div class="alert alert-warning clearfix" role="alert">
                      <strong><i class="fa fa-check mr-3" aria-hidden="true"></i><a href="<?php echo base_url('index.php/artikel') ?>">Masih diproses</a> </strong>
                    </div>

                    <div class="single-preparation-step d-flex mb-3">
                      <h4>04.</h4>
                      <h6 class="ml-4"> Dok, Mengapa ya, berat badan anak saya tidak bisa bertambah walaupun sudah banyak makan dan makanan yang saya sediakan itu makanan 4 sehat 5 sempurna ?
                        <hr>
                        ( 1 Desember, 2019 )
                      </h6>
                    </div>
                    <!-- Alert -->
                    <div class="alert alert-success clearfix" role="alert">
                      <strong><i class="fa fa-check mr-3" aria-hidden="true"></i><a href="<?php echo base_url('index.php/artikel2') ?>">Terjawab ( Klik untuk melihat artikel terkait )</a> </strong>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </section>


  <section class="quote-subscribe-addsn mt-5">
    <div class="container">
      <div class="row align-items-end">
        <!-- ##### Follow Us Instagram Area Start ##### -->
        <?php $this->load->view("_Partials/GaleriEnd.php") ?>
        <!-- ##### Follow Us Instagram Area End ##### -->

        <!-- ##### Footer Area Start ##### -->
        <?php $this->load->view("_Partials/Footer.php") ?>
        <!-- ##### Footer Area Start ##### -->
      </div>
    </div>
  </section>

  <!-- ##### All Javascript Files ##### -->
  <?php $this->load->view("_Partials/Js.php") ?>
</body>

</html>