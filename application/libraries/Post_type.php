<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Post_type
{
    protected $ci;

    private $table = 'post';

    private $supports = [];

    private $id = "";

    public function __construct()
    {
        $this->ci =& get_instance();

        $this->ci->load->library('Form');

    }

    public function support( $supports = []) {

        $this->supports = $supports;

        return $this;

    }

    public function register($id, $name) {
        $this->id = $id;
        $this->post_type[$id] = [
            'slug' => $id,
            'name' => $name
        ];
    }

    public function input($type) 
    { 
    
        foreach($supports as $support) : 
            
            if($support == 'title') :



            endif;

        endforeach;

    }

    public function save() {

    }


}

/* End of file post_type.php */
