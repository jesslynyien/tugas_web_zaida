<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Form
{
    protected $ci;

    private $errors = [];

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function input($type = 'text', $name = "", $label = "", $value = "")
    {
        if($type == 'text') 
        {

            $slug = slugify($name);
            ?>

                <div class="form-group">
                    <label for="i-<?php echo $slug ?>"><?php echo $label ?></label>
                    <input type="text" class="form-control" value="<?php echo $value ?>" id="i-<?php echo $slug ?>">
                </div>

            <?php 

        }

        if($type == 'file') 
        {

            $slug = slugify($name);
            ?>
                <input type="hidden" name="<?php echo $name; ?>" value="<?php echo $value; ?>">
                <div class="form-group">
                
                    <label for="i-<?php echo $slug ?>"><?php echo $label ?></label>
                    <input type="file" name="file_<?php echo $name; ?>" class="form-control" id="i-<?php echo $slug ?>">
                </div>

            <?php 

        }



        return $this;
    }

    public function text($name, $label = "", $value = "") {
        $slug = slugify($name);
        ?>

        <div class="form-group">
            <label for="i-<?php echo $slug ?>"><?php echo $label ?></label>
            <input name="<?php echo $name; ?>" type="text" class="form-control" value="<?php echo $value ?>" id="i-<?php echo $slug ?>">
        </div>

        <?php 
    }

    public function image($name, $label, $value, $thumbnail = "") {

        
        $slug = slugify($name);
        ?>
            <input type="hidden" name="<?php echo $name; ?>" value="<?php echo $value; ?>">
            <div class="form-group">
            
                <label for="i-<?php echo $slug ?>"><?php echo $label ?></label>
                <input type="file" name="file_<?php echo $name; ?>" class="form-control" id="i-<?php echo $slug ?>">
            </div>

            
            <?php if($thumbnail) :  ?>
                <div class="post_thumbnail file_<?php echo $name ?> mb-2">

                    <img class="w-100" src="<?php echo $thumbnail ?>" alt="Thumbnail <?php echo $label ?>" title="<?php echo "Thumbnail {$label}"; ?>">
                
                </div>
            <?php endif ?>

        <?php 

        return $this;
    }

    public function upload($name)
    {
        $config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 4028;
        $config['max_width']            = 4028;
        $config['max_height']           = 4028;

        $this->ci->upload->initialize($config);

        if(isset($_FILES[$name]['tmp_name']) && !empty($_FILES[$name]['tmp_name'])) 
        {
            if($this->ci->upload->do_upload($name)) return $this->ci->upload->data();
            else var_dump($this->ci->upload->display_errors()); return false;
        }
        

        return false;
    }

    public function editor($name, $label = "", $value = "")
    {

        $slug = slugify($name);
        ?>

            <div class="form-group">

                <label for="i-<?php echo $slug ?>"><?php echo $label ?></label>
                <textarea name="<?php echo $slug ?>" id="editor" rows="5"><?php echo $value; ?></textarea>
            </div>

        <?php 

        return $this;

    }

    public function textarea($name, $label = "", $value = "")
    {

        $slug = slugify($name);
        ?>

            <div class="form-group">

                <label for="i-<?php echo $slug ?>"><?php echo $label ?></label>
                <textarea name="<?php echo $slug ?>" id="i-<?php echo $slug ?>" rows="5"><?php echo $value; ?></textarea>
            </div>

        <?php 

        return $this;

    }

    public function get_meta($id_post, $meta_name)
    {
        $meta = $this->ci->db->select()->from('post_meta')->where('id_post', $id_post)->where('post_meta_name', $meta_name)->get();
        if($meta->num_rows() > 0 ) {
            return $meta->row()->post_meta_value;
        }
        return false;
    }

    public function insert_meta($id_post, $meta_name, $meta_value) {
        $this->ci->db->insert('post_meta', [
            'post_meta_value' => $meta_value,
            'post_meta_name' => $meta_name,
            'id_post' => $id_post
        ]);
    }

    public function update_meta($id_post, $meta_name, $meta_value) {
        $this->ci->db->where('id_post', $id_post);
        $this->ci->db->where('post_meta_name', $meta_name);
        $this->ci->db->set('post_meta_value', $meta_value);
        $this->ci->db->update('post_meta');
    }

    public function select($name, $label = "", $options = [], $value = "") {
        $slug = slugify($name);
        ?>

        <div class="form-group">
            <label for="i-<?php echo $slug ?>"><?php echo $label ?></label>
            <select name="<?php echo $name; ?>" id="i-<?php echo $slug; ?>" class="form-control">
                <?php if($options) : ?>

                    <?php foreach($options as $k => $v) : ?>
                        <option value="<?php echo $v ?>" <?php if($v == $value) echo 'selected'; ?>><?php echo $k ?></option>
                    <?php endforeach; ?>

                <?php endif; ?>
            </select>

        
        </div>

        <?php 
    }

}

/* End of file Form.php */
