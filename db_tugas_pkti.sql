-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.2.6-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_tugas_pkti
CREATE DATABASE IF NOT EXISTS `db_tugas_pkti` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_tugas_pkti`;

-- Dumping structure for table db_tugas_pkti.akg
CREATE TABLE IF NOT EXISTS `akg` (
  `No` varchar(10) NOT NULL,
  `GolUsia` varchar(20) DEFAULT NULL,
  `BB_kg` int(11) DEFAULT NULL,
  `TB_cm` int(11) DEFAULT NULL,
  `Energi_kkal` int(11) DEFAULT NULL,
  `Protein_g` int(11) DEFAULT NULL,
  `Lemak_g` int(11) DEFAULT NULL,
  `Karbo_g` int(11) DEFAULT NULL,
  PRIMARY KEY (`No`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_tugas_pkti.akg: ~0 rows (approximately)
/*!40000 ALTER TABLE `akg` DISABLE KEYS */;
/*!40000 ALTER TABLE `akg` ENABLE KEYS */;

-- Dumping structure for table db_tugas_pkti.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id_cat` int(11) NOT NULL AUTO_INCREMENT,
  `cat_slug` varchar(100) NOT NULL,
  `cat_name` varchar(50) NOT NULL,
  `cat_desc` text NOT NULL,
  `cat_parent` int(11) NOT NULL,
  `cat_type` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_cat`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_tugas_pkti.categories: ~7 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id_cat`, `cat_slug`, `cat_name`, `cat_desc`, `cat_parent`, `cat_type`) VALUES
	(7, 'makanan', 'Makanan', 'Kuliner berupa makanan', 0, 'kuliner'),
	(8, 'minuman', 'Minuman', 'Kuliner berupa minuman', 0, 'kuliner'),
	(9, 'halal', 'Halal', 'Kuliner berupa makanan halal', 0, 'kuliner'),
	(10, 'non-halal', 'Non-Halal', 'Kuliner berupa makanan non-halal', 0, 'kuliner'),
	(11, 'vegetarian', 'Vegetarian', 'Kuliner berupa makanan vegetarian ( tanpa daging)', 0, 'kuliner'),
	(12, 'homepage-slider', 'Homepage Slider', 'ini adalah homapage slider', 0, 'slider'),
	(15, 'info-makanan', 'Info Makanan', 'category tentang info makanan', 0, 'category');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table db_tugas_pkti.faq
CREATE TABLE IF NOT EXISTS `faq` (
  `id_faq` varchar(10) NOT NULL,
  `faq_title` varchar(100) NOT NULL,
  `faq_ts` varchar(100) NOT NULL,
  `faq_date` date NOT NULL,
  `faq_age` varchar(100) NOT NULL,
  `faq_content` text NOT NULL,
  `faq_answer` text NOT NULL DEFAULT '',
  `faq_status` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_faq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_tugas_pkti.faq: ~1 rows (approximately)
/*!40000 ALTER TABLE `faq` DISABLE KEYS */;
INSERT INTO `faq` (`id_faq`, `faq_title`, `faq_ts`, `faq_date`, `faq_age`, `faq_content`, `faq_answer`, `faq_status`) VALUES
	('', 'qq', 'qq', '0000-00-00', '3', 'qq', '#', 'pending');
/*!40000 ALTER TABLE `faq` ENABLE KEYS */;

-- Dumping structure for table db_tugas_pkti.kuliner
CREATE TABLE IF NOT EXISTS `kuliner` (
  `ID_Kul` varchar(10) NOT NULL,
  `Nama` varchar(100) NOT NULL,
  `UrlGambar` varchar(200) NOT NULL,
  `JlhView` int(11) DEFAULT NULL,
  `FaktaUnik` varchar(1000) DEFAULT NULL,
  `Bumil` varchar(1) DEFAULT NULL,
  `Diabetes` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ID_Kul`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_tugas_pkti.kuliner: ~1 rows (approximately)
/*!40000 ALTER TABLE `kuliner` DISABLE KEYS */;
INSERT INTO `kuliner` (`ID_Kul`, `Nama`, `UrlGambar`, `JlhView`, `FaktaUnik`, `Bumil`, `Diabetes`) VALUES
	('F001', 'Nasi Goreng', 'https://blue.kumparan.com/image/upload/fl_progressive,fl_lossy,c_fill,q_auto:best,w_640/v1515557737/asxtrr2ga1os4abfmuoe.jpg', 0, 'Berkalori Tinggi, Berbahaya jika dimakan bersama Mentimun', 'Y', 'Y');
/*!40000 ALTER TABLE `kuliner` ENABLE KEYS */;

-- Dumping structure for table db_tugas_pkti.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) DEFAULT NULL,
  `menu_url` text DEFAULT NULL,
  `menu_parent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table db_tugas_pkti.menus: ~1 rows (approximately)
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` (`id_menu`, `menu_name`, `menu_url`, `menu_parent`) VALUES
	(14, 'Beranda', 'http://tugas_web_zaida.test/', 0);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

-- Dumping structure for table db_tugas_pkti.pengguna
CREATE TABLE IF NOT EXISTS `pengguna` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `password_user` varchar(32) DEFAULT NULL,
  `fullname_user` varchar(100) NOT NULL,
  `id_role` int(11) NOT NULL,
  `pengguna_status` int(1) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_tugas_pkti.pengguna: ~3 rows (approximately)
/*!40000 ALTER TABLE `pengguna` DISABLE KEYS */;
INSERT INTO `pengguna` (`id_user`, `username`, `email_user`, `password_user`, `fullname_user`, `id_role`, `pengguna_status`) VALUES
	(1, 'superuser', 'superuser@gmail.com', '0baea2f0ae20150db78f58cddac442a9', 'Super User', 1, 1),
	(2, 'bagas', 'bagas.topati@gmail.com', 'ee776a18253721efe8a62e4abd29dc47', '', 2, 1),
	(3, 'bambang', 'bambang@gmail.com', 'a9711cbb2e3c2d5fc97a63e45bbe5076', '', 2, 1);
/*!40000 ALTER TABLE `pengguna` ENABLE KEYS */;

-- Dumping structure for table db_tugas_pkti.penjual
CREATE TABLE IF NOT EXISTS `penjual` (
  `ID_Seller` varchar(10) NOT NULL,
  `Nama` varchar(100) NOT NULL,
  `Alamat` varchar(100) NOT NULL,
  `LinkPeta` varchar(500) DEFAULT NULL,
  `LinkGbr` varchar(500) DEFAULT NULL,
  `NoTelp` varchar(20) DEFAULT NULL,
  `Waktu` varchar(100) DEFAULT NULL,
  `Rating` double DEFAULT NULL,
  PRIMARY KEY (`ID_Seller`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_tugas_pkti.penjual: ~0 rows (approximately)
/*!40000 ALTER TABLE `penjual` DISABLE KEYS */;
/*!40000 ALTER TABLE `penjual` ENABLE KEYS */;

-- Dumping structure for table db_tugas_pkti.penjual_kuliner
CREATE TABLE IF NOT EXISTS `penjual_kuliner` (
  `ID_Kul` varchar(10) DEFAULT NULL,
  `ID_Seller` varchar(10) DEFAULT NULL,
  KEY `ID_Kul` (`ID_Kul`),
  KEY `ID_Seller` (`ID_Seller`),
  CONSTRAINT `penjual_kuliner_ibfk_1` FOREIGN KEY (`ID_Kul`) REFERENCES `kuliner` (`ID_Kul`) ON DELETE CASCADE,
  CONSTRAINT `penjual_kuliner_ibfk_2` FOREIGN KEY (`ID_Seller`) REFERENCES `penjual` (`ID_Seller`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_tugas_pkti.penjual_kuliner: ~0 rows (approximately)
/*!40000 ALTER TABLE `penjual_kuliner` DISABLE KEYS */;
/*!40000 ALTER TABLE `penjual_kuliner` ENABLE KEYS */;

-- Dumping structure for table db_tugas_pkti.post
CREATE TABLE IF NOT EXISTS `post` (
  `id_post` int(11) NOT NULL AUTO_INCREMENT,
  `post_title` varchar(200) DEFAULT NULL,
  `post_slug` varchar(200) DEFAULT NULL,
  `post_content` text DEFAULT NULL,
  `post_date` date DEFAULT NULL,
  `post_author` varchar(100) DEFAULT NULL,
  `post_type` varchar(100) DEFAULT NULL,
  `post_thumbnail` text DEFAULT NULL,
  PRIMARY KEY (`id_post`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_tugas_pkti.post: ~10 rows (approximately)
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` (`id_post`, `post_title`, `post_slug`, `post_content`, `post_date`, `post_author`, `post_type`, `post_thumbnail`) VALUES
	(2, 'Bakwan', 'bakwan', '<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eius odit illo nihil, consequatur eos, cumque, aspernatur ut ad voluptatum amet aut? Impedit eos non in aut maxime officiis odio sapiente sit. Sit, similique a. Labore repellendus amet architecto? Quae facilis facere soluta vel, expedita cupiditate aliquam, saepe voluptatem incidunt, aperiam eaque repellendus quibusdam quas distinctio ratione? Esse earum quam quisquam assumenda! Iste ad sed consequuntur laboriosam ea, officia natus ipsum asperiores eius, culpa consectetur debitis quis odit aperiam fugiat totam, aut vero sequi ipsam impedit unde. Ducimus, magnam neque placeat dolorem quasi debitis excepturi sunt pariatur id harum. Officia rem tempora suscipit? Quisquam numquam voluptatem sit! Consequatur at iure laborum ex doloremque cumque expedita dolor officiis omnis, labore, rem maiores obcaecati delectus quibusdam harum hic, provident numquam molestiae iusto dolorum similique quos tenetur. Accusantium est quis delectus odio quas sint qui voluptatibus labore reiciendis tenetur repellendus, aspernatur saepe ullam repellat porro quidem natus vel aperiam necessitatibus? Temporibus vitae nulla sit soluta eos aperiam sint necessitatibus sequi facere ipsa eum accusantium, consequuntur illo, alias voluptate inventore amet laudantium odit! Excepturi quo veritatis aliquid perferendis quod dolorem! Minus incidunt aliquid assumenda!</p>\r\n<p>&nbsp;</p>\r\n<p>Debitis optio vitae maiores dolore sapiente sit pariatur eveniet magni maxime quaerat ex est mollitia ab ducimus adipisci, earum obcaecati exercitationem culpa omnis. Tempore sunt placeat aut provident explicabo voluptates, qui perspiciatis sed atque minima velit eveniet nisi ratione iusto? Possimus a repellendus ipsum ullam sapiente asperiores fuga ducimus exercitationem, saepe voluptas vel eum impedit, hic, amet assumenda excepturi. Blanditiis consectetur natus ipsam dolorum molestias? Aliquid inventore quia consequuntur, quam commodi ratione eum. Ipsam est deserunt consequatur iste dolor sed, aperiam ea maiores sapiente esse ipsum, voluptas architecto ad? Consequuntur, ipsum voluptas autem itaque ratione at nam iste, eligendi cumque adipisci ad nisi libero odit doloremque enim rem est error sapiente aspernatur harum ipsam placeat alias! Illo reprehenderit dolorum ratione recusandae ea doloremque eveniet provident, laborum sunt blanditiis iure voluptatibus in eum ab vero commodi modi accusamus libero odio! Nulla, dolore libero quisquam nam debitis molestias pariatur! Quibusdam saepe, ratione temporibus a perspiciatis voluptatibus dolor odio quas facere, veritatis molestias cupiditate qui doloribus sit eum. Maxime quod alias eligendi aperiam, saepe voluptatum, assumenda pariatur deserunt, dolore doloremque iusto? Quas aliquam deleniti sit fugiat nulla dolor ab eveniet, fuga labore voluptatibus neque ipsum repudiandae accusantium autem necessitatibus ad accusamus? Facilis sapiente mollitia provident laboriosam quas recusandae, consequuntur magni fuga, sequi molestias quae! Reiciendis hic magnam, quasi consequatur placeat illum blanditiis repellendus officiis iusto? Odit alias vel recusandae hic dicta error quam, sit ut voluptatum asperiores minima! Odio, quae dolor magni id assumenda nisi nihil incidunt est suscipit nulla quidem asperiores, odit nobis pariatur sunt molestiae ipsa? Praesentium, eveniet fugiat nihil mollitia, autem commodi dicta modi at quaerat reprehenderit sint placeat omnis, doloribus hic impedit repellendus facere sequi sunt ea quidem magnam consequatur. Hic tempore ipsa soluta fugiat impedit officiis quam natus non in voluptate veritatis quia id eaque, consequatur molestiae at aliquam debitis eligendi corrupti a. Dolor tempore maxime ratione laudantium nam!</p>', '2020-01-07', '1', 'kuliner', 'bakwan3.jpg'),
	(5, 'Pisang ', 'pisang', '<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eius odit illo nihil, consequatur eos, cumque, aspernatur ut ad voluptatum amet aut? Impedit eos non in aut maxime officiis odio sapiente sit. Sit, similique a. Labore repellendus amet architecto? Quae facilis facere soluta vel, expedita cupiditate aliquam, saepe voluptatem incidunt, aperiam eaque repellendus quibusdam quas distinctio ratione? Esse earum quam quisquam assumenda! Iste ad sed consequuntur laboriosam ea, officia natus ipsum asperiores eius, culpa consectetur debitis quis odit aperiam fugiat totam, aut vero sequi ipsam impedit unde. Ducimus, magnam neque placeat dolorem quasi debitis excepturi sunt pariatur id harum. Officia rem tempora suscipit? Quisquam numquam voluptatem sit! Consequatur at iure laborum ex doloremque cumque expedita dolor officiis omnis, labore, rem maiores obcaecati delectus quibusdam harum hic, provident numquam molestiae iusto dolorum similique quos tenetur. Accusantium est quis delectus odio quas sint qui voluptatibus labore reiciendis tenetur repellendus, aspernatur saepe ullam repellat porro quidem natus vel aperiam necessitatibus? Temporibus vitae nulla sit soluta eos aperiam sint necessitatibus sequi facere ipsa eum accusantium, consequuntur illo, alias voluptate inventore amet laudantium odit! Excepturi quo veritatis aliquid perferendis quod dolorem! Minus incidunt aliquid assumenda!</p>\r\n<p>&nbsp;</p>\r\n<p>Debitis optio vitae maiores dolore sapiente sit pariatur eveniet magni maxime quaerat ex est mollitia ab ducimus adipisci, earum obcaecati exercitationem culpa omnis. Tempore sunt placeat aut provident explicabo voluptates, qui perspiciatis sed atque minima velit eveniet nisi ratione iusto? Possimus a repellendus ipsum ullam sapiente asperiores fuga ducimus exercitationem, saepe voluptas vel eum impedit, hic, amet assumenda excepturi. Blanditiis consectetur natus ipsam dolorum molestias? Aliquid inventore quia consequuntur, quam commodi ratione eum. Ipsam est deserunt consequatur iste dolor sed, aperiam ea maiores sapiente esse ipsum, voluptas architecto ad? Consequuntur, ipsum voluptas autem itaque ratione at nam iste, eligendi cumque adipisci ad nisi libero odit doloremque enim rem est error sapiente aspernatur harum ipsam placeat alias! Illo reprehenderit dolorum ratione recusandae ea doloremque eveniet provident, laborum sunt blanditiis iure voluptatibus in eum ab vero commodi modi accusamus libero odio! Nulla, dolore libero quisquam nam debitis molestias pariatur! Quibusdam saepe, ratione temporibus a perspiciatis voluptatibus dolor odio quas facere, veritatis molestias cupiditate qui doloribus sit eum. Maxime quod alias eligendi aperiam, saepe voluptatum, assumenda pariatur deserunt, dolore doloremque iusto? Quas aliquam deleniti sit fugiat nulla dolor ab eveniet, fuga labore voluptatibus neque ipsum repudiandae accusantium autem necessitatibus ad accusamus? Facilis sapiente mollitia provident laboriosam quas recusandae, consequuntur magni fuga, sequi molestias quae! Reiciendis hic magnam, quasi consequatur placeat illum blanditiis repellendus officiis iusto? Odit alias vel recusandae hic dicta error quam, sit ut voluptatum asperiores minima! Odio, quae dolor magni id assumenda nisi nihil incidunt est suscipit nulla quidem asperiores, odit nobis pariatur sunt molestiae ipsa? Praesentium, eveniet fugiat nihil mollitia, autem commodi dicta modi at quaerat reprehenderit sint placeat omnis, doloribus hic impedit repellendus facere sequi sunt ea quidem magnam consequatur. Hic tempore ipsa soluta fugiat impedit officiis quam natus non in voluptate veritatis quia id eaque, consequatur molestiae at aliquam debitis eligendi corrupti a. Dolor tempore maxime ratione laudantium nam!</p>', '2020-01-07', '1', 'kuliner', ''),
	(6, 'Mie goreng', 'mie-goreng', '<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eius odit illo nihil, consequatur eos, cumque, aspernatur ut ad voluptatum amet aut? Impedit eos non in aut maxime officiis odio sapiente sit. Sit, similique a. Labore repellendus amet architecto? Quae facilis facere soluta vel, expedita cupiditate aliquam, saepe voluptatem incidunt, aperiam eaque repellendus quibusdam quas distinctio ratione? Esse earum quam quisquam assumenda! Iste ad sed consequuntur laboriosam ea, officia natus ipsum asperiores eius, culpa consectetur debitis quis odit aperiam fugiat totam, aut vero sequi ipsam impedit unde. Ducimus, magnam neque placeat dolorem quasi debitis excepturi sunt pariatur id harum. Officia rem tempora suscipit? Quisquam numquam voluptatem sit! Consequatur at iure laborum ex doloremque cumque expedita dolor officiis omnis, labore, rem maiores obcaecati delectus quibusdam harum hic, provident numquam molestiae iusto dolorum similique quos tenetur. Accusantium est quis delectus odio quas sint qui voluptatibus labore reiciendis tenetur repellendus, aspernatur saepe ullam repellat porro quidem natus vel aperiam necessitatibus? Temporibus vitae nulla sit soluta eos aperiam sint necessitatibus sequi facere ipsa eum accusantium, consequuntur illo, alias voluptate inventore amet laudantium odit! Excepturi quo veritatis aliquid perferendis quod dolorem! Minus incidunt aliquid assumenda!</p>\r\n<p>&nbsp;</p>\r\n<p>Debitis optio vitae maiores dolore sapiente sit pariatur eveniet magni maxime quaerat ex est mollitia ab ducimus adipisci, earum obcaecati exercitationem culpa omnis. Tempore sunt placeat aut provident explicabo voluptates, qui perspiciatis sed atque minima velit eveniet nisi ratione iusto? Possimus a repellendus ipsum ullam sapiente asperiores fuga ducimus exercitationem, saepe voluptas vel eum impedit, hic, amet assumenda excepturi. Blanditiis consectetur natus ipsam dolorum molestias? Aliquid inventore quia consequuntur, quam commodi ratione eum. Ipsam est deserunt consequatur iste dolor sed, aperiam ea maiores sapiente esse ipsum, voluptas architecto ad? Consequuntur, ipsum voluptas autem itaque ratione at nam iste, eligendi cumque adipisci ad nisi libero odit doloremque enim rem est error sapiente aspernatur harum ipsam placeat alias! Illo reprehenderit dolorum ratione recusandae ea doloremque eveniet provident, laborum sunt blanditiis iure voluptatibus in eum ab vero commodi modi accusamus libero odio! Nulla, dolore libero quisquam nam debitis molestias pariatur! Quibusdam saepe, ratione temporibus a perspiciatis voluptatibus dolor odio quas facere, veritatis molestias cupiditate qui doloribus sit eum. Maxime quod alias eligendi aperiam, saepe voluptatum, assumenda pariatur deserunt, dolore doloremque iusto? Quas aliquam deleniti sit fugiat nulla dolor ab eveniet, fuga labore voluptatibus neque ipsum repudiandae accusantium autem necessitatibus ad accusamus? Facilis sapiente mollitia provident laboriosam quas recusandae, consequuntur magni fuga, sequi molestias quae! Reiciendis hic magnam, quasi consequatur placeat illum blanditiis repellendus officiis iusto? Odit alias vel recusandae hic dicta error quam, sit ut voluptatum asperiores minima! Odio, quae dolor magni id assumenda nisi nihil incidunt est suscipit nulla quidem asperiores, odit nobis pariatur sunt molestiae ipsa? Praesentium, eveniet fugiat nihil mollitia, autem commodi dicta modi at quaerat reprehenderit sint placeat omnis, doloribus hic impedit repellendus facere sequi sunt ea quidem magnam consequatur. Hic tempore ipsa soluta fugiat impedit officiis quam natus non in voluptate veritatis quia id eaque, consequatur molestiae at aliquam debitis eligendi corrupti a. Dolor tempore maxime ratione laudantium nam!</p>', '2020-01-07', '1', 'kuliner', ''),
	(14, 'Otak Otak', 'otak-otak', '<p>Otak-otak adalah makanan yang terbuat dari daging tenggiri cincang yang dibungkus dengan daun pisang, dipanggang, dan disajikan dengan kuah asam pedas. Kuliner yang satu ini kaya akan protein dan sangat bagus untuk pertumbuhan. Selain itu, kuliner yang satu ini juga mengandung banyak vitamin. Lezat dan dan bikin nagih.</p>', '2020-01-07', '1', 'slider', 'bakwan5.jpg'),
	(15, 'Bihun Bebek Obat', 'bihun-bebek-obat', '<p>Bihun bebek Obat Medan menjadi salah satu kuliner sehat yang wajib anda coba. Diracik dengan obat-obatan herbal membuatnya sangat bagus untuk kesehatan tubuh.</p>', '2020-01-07', '1', 'slider', 'bg2.jpg'),
	(16, 'Sate padang', 'sate-padang', '<p>Rekomendasi kuliner khas dan legendaris di Medan Sumatra Utara yang menarik untuk kamu coba.</p>\r\n<p>Rekomendasi kuliner Medan terkenal dengan berbagai macam menu makanan yang beragam dan menggugah selera.</p>\r\n<p>Jika bingung mencari rekomendasi kuliner khas Medan bisa mencoba sarapan dengan Mie Balap atau Soto Kesawan yang legendaris.</p>', '2020-01-07', '1', 'slider', '4sehat.jpg'),
	(18, 'Tentang Kami', 'tentang-kami', '<p>Kami ada tim 7 era modern.</p>', '2020-01-07', '1', 'page', NULL),
	(19, 'wew', 'wew', '<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eius odit illo nihil, consequatur eos, cumque, aspernatur ut ad voluptatum amet aut? Impedit eos non in aut maxime officiis odio sapiente sit. Sit, similique a. Labore repellendus amet architecto? Quae facilis facere soluta vel, expedita cupiditate aliquam, saepe voluptatem incidunt, aperiam eaque repellendus quibusdam quas distinctio ratione? Esse earum quam quisquam assumenda! Iste ad sed consequuntur laboriosam ea, officia natus ipsum asperiores eius, culpa consectetur debitis quis odit aperiam fugiat totam, aut vero sequi ipsam impedit unde. Ducimus, magnam neque placeat dolorem quasi debitis excepturi sunt pariatur id harum. Officia rem tempora suscipit? Quisquam numquam voluptatem sit! Consequatur at iure laborum ex doloremque cumque expedita dolor officiis omnis, labore, rem maiores obcaecati delectus quibusdam harum hic, provident numquam molestiae iusto dolorum similique quos tenetur. Accusantium est quis delectus odio quas sint qui voluptatibus labore reiciendis tenetur repellendus, aspernatur saepe ullam repellat porro quidem natus vel aperiam necessitatibus? Temporibus vitae nulla sit soluta eos aperiam sint necessitatibus sequi facere ipsa eum accusantium, consequuntur illo, alias voluptate inventore amet laudantium odit! Excepturi quo veritatis aliquid perferendis quod dolorem! Minus incidunt aliquid assumenda!</p>\r\n<p>&nbsp;</p>\r\n<p>Debitis optio vitae maiores dolore sapiente sit pariatur eveniet magni maxime quaerat ex est mollitia ab ducimus adipisci, earum obcaecati exercitationem culpa omnis. Tempore sunt placeat aut provident explicabo voluptates, qui perspiciatis sed atque minima velit eveniet nisi ratione iusto? Possimus a repellendus ipsum ullam sapiente asperiores fuga ducimus exercitationem, saepe voluptas vel eum impedit, hic, amet assumenda excepturi. Blanditiis consectetur natus ipsam dolorum molestias? Aliquid inventore quia consequuntur, quam commodi ratione eum. Ipsam est deserunt consequatur iste dolor sed, aperiam ea maiores sapiente esse ipsum, voluptas architecto ad? Consequuntur, ipsum voluptas autem itaque ratione at nam iste, eligendi cumque adipisci ad nisi libero odit doloremque enim rem est error sapiente aspernatur harum ipsam placeat alias! Illo reprehenderit dolorum ratione recusandae ea doloremque eveniet provident, laborum sunt blanditiis iure voluptatibus in eum ab vero commodi modi accusamus libero odio! Nulla, dolore libero quisquam nam debitis molestias pariatur! Quibusdam saepe, ratione temporibus a perspiciatis voluptatibus dolor odio quas facere, veritatis molestias cupiditate qui doloribus sit eum. Maxime quod alias eligendi aperiam, saepe voluptatum, assumenda pariatur deserunt, dolore doloremque iusto? Quas aliquam deleniti sit fugiat nulla dolor ab eveniet, fuga labore voluptatibus neque ipsum repudiandae accusantium autem necessitatibus ad accusamus? Facilis sapiente mollitia provident laboriosam quas recusandae, consequuntur magni fuga, sequi molestias quae! Reiciendis hic magnam, quasi consequatur placeat illum blanditiis repellendus officiis iusto? Odit alias vel recusandae hic dicta error quam, sit ut voluptatum asperiores minima! Odio, quae dolor magni id assumenda nisi nihil incidunt est suscipit nulla quidem asperiores, odit nobis pariatur sunt molestiae ipsa? Praesentium, eveniet fugiat nihil mollitia, autem commodi dicta modi at quaerat reprehenderit sint placeat omnis, doloribus hic impedit repellendus facere sequi sunt ea quidem magnam consequatur. Hic tempore ipsa soluta fugiat impedit officiis quam natus non in voluptate veritatis quia id eaque, consequatur molestiae at aliquam debitis eligendi corrupti a. Dolor tempore maxime ratione laudantium nam!</p>', '2020-01-07', '1', 'kuliner', ''),
	(22, 'ini adalah artikel pertama saya', 'ini-adalah-artikel-pertama-saya', '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla minus cum facere quasi cumque, sed at impedit quae commodi perspiciatis in. Distinctio provident voluptatibus sed ipsam nobis placeat, ipsa reprehenderit?</p>', '2020-01-06', '1', 'post', 'bakwan4.jpg');
/*!40000 ALTER TABLE `post` ENABLE KEYS */;

-- Dumping structure for table db_tugas_pkti.post_meta
CREATE TABLE IF NOT EXISTS `post_meta` (
  `id_post_meta` int(11) NOT NULL AUTO_INCREMENT,
  `id_post` int(11) NOT NULL,
  `post_meta_name` text NOT NULL,
  `post_meta_value` text NOT NULL,
  PRIMARY KEY (`id_post_meta`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_tugas_pkti.post_meta: ~31 rows (approximately)
/*!40000 ALTER TABLE `post_meta` DISABLE KEYS */;
INSERT INTO `post_meta` (`id_post_meta`, `id_post`, `post_meta_name`, `post_meta_value`) VALUES
	(1, 21, 'kalori', '100'),
	(2, 21, 'kalori', '100'),
	(3, 21, 'lemak', '3'),
	(4, 21, 'karbohidrat', '3'),
	(5, 21, 'protein', '3'),
	(6, 2, 'kalori', '10'),
	(7, 2, 'lemak', '20'),
	(8, 2, 'karbohidrat', '30'),
	(9, 2, 'protein', '40'),
	(10, 2, 'rating', '5'),
	(11, 5, 'kalori', ''),
	(12, 5, 'lemak', ''),
	(13, 5, 'karbohidrat', ''),
	(14, 5, 'protein', ''),
	(15, 5, 'rating', ''),
	(16, 6, 'kalori', ''),
	(17, 6, 'lemak', ''),
	(18, 6, 'karbohidrat', ''),
	(19, 6, 'protein', ''),
	(20, 6, 'rating', ''),
	(21, 19, 'kalori', ''),
	(22, 19, 'lemak', ''),
	(23, 19, 'karbohidrat', ''),
	(24, 19, 'protein', ''),
	(25, 19, 'rating', ''),
	(26, 14, 'btn_cta_name', 'SEE RECEIPT'),
	(27, 14, 'btn_cta_url', '#'),
	(28, 14, 'btn_cta_target', '_self'),
	(29, 14, 'btn_cta_name', 'SEE RECEIPT'),
	(30, 14, 'btn_cta_url', '#'),
	(31, 14, 'btn_cta_target', '_self'),
	(32, 15, 'btn_cta_name', 'LIHAT'),
	(33, 15, 'btn_cta_url', '#'),
	(34, 15, 'btn_cta_target', '_self'),
	(35, 15, 'btn_cta_name', 'LIHAT'),
	(36, 15, 'btn_cta_url', '#'),
	(37, 15, 'btn_cta_target', '_self'),
	(38, 16, 'btn_cta_name', 'See Receipe'),
	(39, 16, 'btn_cta_url', '#'),
	(40, 16, 'btn_cta_target', '_self'),
	(41, 16, 'btn_cta_name', 'See Receipe'),
	(42, 16, 'btn_cta_url', '#'),
	(43, 16, 'btn_cta_target', '_self'),
	(44, 15, 'btn_cta_name', 'LIHAT'),
	(45, 15, 'btn_cta_url', '#'),
	(46, 15, 'btn_cta_target', '_self');
/*!40000 ALTER TABLE `post_meta` ENABLE KEYS */;

-- Dumping structure for table db_tugas_pkti.post_taxonomy
CREATE TABLE IF NOT EXISTS `post_taxonomy` (
  `id_post` int(11) DEFAULT NULL,
  `id_cat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table db_tugas_pkti.post_taxonomy: ~13 rows (approximately)
/*!40000 ALTER TABLE `post_taxonomy` DISABLE KEYS */;
INSERT INTO `post_taxonomy` (`id_post`, `id_cat`) VALUES
	(2, 7),
	(2, 8),
	(2, 10),
	(19, 7),
	(19, 8),
	(19, 9),
	(19, 10),
	(19, 11),
	(22, 15),
	(14, 12),
	(16, 12),
	(15, 12);
/*!40000 ALTER TABLE `post_taxonomy` ENABLE KEYS */;

-- Dumping structure for table db_tugas_pkti.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(30) NOT NULL,
  `role_desc` text NOT NULL,
  `role_cap` int(2) NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table db_tugas_pkti.roles: ~1 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id_role`, `role_name`, `role_desc`, `role_cap`) VALUES
	(1, 'Administrator', '', 99),
	(2, 'Member', ' ', 30);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
